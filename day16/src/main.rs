use std::path::PathBuf;
use std::mem::replace;
use std::io::{Read, Write};
use std::fs::File;
use std::collections::{BTreeMap, VecDeque, HashSet, HashMap};
use regex::{Regex, Match};
use anyhow::{Result, Context, anyhow};
use std::str::Chars;
use std::iter::Peekable;
use std::cmp::Ordering;
use rayon::prelude::*;

mod utils;
use utils::*;

fn main() {
	let input: String = {
		let input_path = std::env::args().nth(1).unwrap_or("input.txt".to_string());
		let mut input_file = File::open(input_path).expect("Failed to open the input file");
		let mut input = String::new();
		input_file.read_to_string(&mut input).expect("Failed to read the input");
		input = input.trim().to_string();
		input
	};

	let lines = input.lines().collect::<Vec<_>>();

	let valves: BTreeMap<_, _> = lines.iter().map(|line| {
		let bits = line.split(" ").vec();
		let name = bits[1];
		let flow_rate = &bits[4][5..];
		let flow_rate: usize = flow_rate[..flow_rate.len()-1].parse().unwrap();
		let tunnels_to = bits.into_iter()
			.skip(9)
			.map(|tunnel| if tunnel.chars().last() == Some(',') { &tunnel[..tunnel.len()-1] } else { tunnel } )
			.vec();
		(name.clone(), Valve {
			name, flow_rate, tunnels_to
		})
	}).collect();

	let mut paths = vec![vec![Move("AA")]];
	let mut best_pressure: usize = 0;
	while !paths.is_empty() {
		do_paths(&mut paths, &valves, &mut best_pressure);
	}

	let mut paths = vec![vec![(Move("AA"), Move("AA"))]];
	let mut best_pressure: usize = 0;
	/*while !paths.is_empty() {
		do_paths_with_elephant(&mut paths, &valves, &mut best_pressure);
	}*/
	fancy_algorithm_with_elephant(&valves);
}

fn do_paths<'a>(paths: &mut Vec<Vec<Action<'a>>>, valves: &BTreeMap<&str, Valve<'a>>, best_pressure: &mut usize) {
	for path in replace(paths, Vec::new()) {
		if path.len() == 31 {
			let total_pressure_released = calculate_total_released(&path, valves);
			if total_pressure_released > *best_pressure {
				*best_pressure = total_pressure_released;
				println!("{:?}", path);
				println!("New best pressure: {}", total_pressure_released);
			}
		} else {
			let loc = path.last().unwrap().loc();
			let move_to = &valves[loc].tunnels_to;

			let open_action = Open(loc);
			let can_open = path.iter().filter(|a| a.loc() == loc).count() <= 1;

			if can_open {
				let mut path = path.clone();
				path.push(open_action);
				paths.push(path);
			}
			for move_to in move_to {
				let mut path = path.clone();
				path.push(Move(move_to));
				paths.push(path);
			}
		}
	}
	if paths.len() > 100 {
		paths.sort_by_key(|path| calculate_total_released(path, valves));
		paths.reverse();
		drop(paths.splice(100.., None));
		if let Some(path) = paths.first() {
			println!("{}", calculate_total_released(path, valves));
		}
	}
}

fn calculate_total_released<'a>(path: &Vec<Action<'a>>, valves: &BTreeMap<&str, Valve<'a>>) -> usize {
	let mut pressure_releasing = 0;
	let mut total_pressure_released = 0;
	for action in path {
		total_pressure_released += pressure_releasing;
		if let Open(loc) = action {
			let valve = &valves[loc];
			pressure_releasing += valve.flow_rate;
		}
	}
	total_pressure_released
}

fn do_paths_with_elephant<'a>(paths: &mut Vec<Vec<(Action<'a>, Action<'a>)>>, valves: &BTreeMap<&str, Valve<'a>>, best_pressure: &mut usize) {
	let tasks = replace(paths, Vec::new());
	*paths = tasks.par_iter().map(|path| {
		let mut paths = Vec::new();
		let (a, b) = path.last().unwrap();
		let actions = [a, b].into_iter().map(|last_action| {
			let loc = last_action.loc();
			let move_to = &valves[loc].tunnels_to;
			let mut actions = vec![];

			let open_action = Open(loc);
			let can_open = path.iter().filter(|(a, b)| a == &Open(loc) || b == &Open(loc)).count() == 0;

			if can_open { actions.push(open_action); }
			for move_to in move_to { actions.push(Move(move_to)); }
			actions
		}).vec();
		for a in &actions[0] {
			for b in &actions[1] {
				if a == b && matches!(a, Open(_)) { continue }
				let mut path = path.clone();
				path.push((a.clone(), b.clone()));
				paths.push(path);
			}
		}

		let mut best_pressure = 0;
		let paths = paths.into_iter().filter(|path| {
			if path.len() == 26 {
				let total_pressure_released = calculate_total_released_with_elephant(&path, valves);
				if total_pressure_released > best_pressure {
					best_pressure = total_pressure_released;
				}
				false
			}
			else { true }
		}).vec();

		(paths, best_pressure)
	})
		.collect::<Vec<_>>()
		.into_iter()
		.map(|(paths, local_best_pressure)| { *best_pressure = (*best_pressure).max(local_best_pressure); paths })
		.map(|v| v.into_iter())
		.flatten()
		.vec();
	let limit = 100000;
	if paths.len() > limit {
		paths.as_mut_slice().par_sort_by_key(|path| calculate_total_released_with_elephant(path, valves));
		paths.reverse();
		drop(paths.splice(limit.., None));
		if let Some(path) = paths.first() {
			println!("{:?}", path);
			println!("New best pressure: {}", calculate_total_released_with_elephant(path, valves));
			println!("New best pressure: {}", best_pressure);
		}
	}
}
fn calculate_total_released_with_elephant<'a>(path: &Vec<(Action<'a>, Action<'a>)>, valves: &BTreeMap<&str, Valve<'a>>) -> usize {
	let mut pressure_releasing = 0;
	let mut total_pressure_released = 0;
	for action in path {
		total_pressure_released += pressure_releasing;
		if let Open(loc) = action.0 {
			let valve = &valves[loc];
			pressure_releasing += valve.flow_rate;
		}
		if let Open(loc) = action.1 {
			let valve = &valves[loc];
			pressure_releasing += valve.flow_rate;
		}
	}
	total_pressure_released
}

fn fancy_algorithm_with_elephant<'a>(valves: &BTreeMap<&str, Valve<'a>>) {
	#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
	struct Node<'a> {
		valve_h: &'a str,
		valve_e: &'a str,
		pressure_releasing: usize,
	}
	#[derive(Debug, Clone)]
	struct Head<'a> {
		node: Node<'a>,
		time: usize,
		pressure_released: usize,
		valves_opened: Vec<&'a str>,
		actions_h: Vec<Action<'a>>,
		actions_e: Vec<Action<'a>>,
	}

	let mut nodes = BTreeMap::<Node<'a>, usize>::new();
	let mut heads = vec![Head {
		node: Node {
			valve_h: "AA",
			valve_e: "AA",
			pressure_releasing: 0,
		},
		time: 0,
		pressure_released: 0,
		valves_opened: Vec::new(),
		actions_h: vec![],
		actions_e: vec![],
	}];

	while !heads.is_empty() && heads.first().map(|head| head.time <= 26) == Some(true) {
		for head in replace(&mut heads, Vec::new()) {
			let time = head.time + 1;
			let pressure_released = head.pressure_released + head.node.pressure_releasing;

			let gen_new_heads = |head: &Head<'a>, is_e: bool| {
				let mut new_heads = Vec::new();
				let loc = if is_e { head.node.valve_e } else { head.node.valve_h };
				let valve = &valves[loc];
				if !head.valves_opened.contains(&loc) {
					//Open the valve
					let mut node = head.node;
					node.pressure_releasing += valve.flow_rate;
					let mut valves_opened = head.valves_opened.clone();
					valves_opened.push(loc);

					let mut new_actions = if is_e { head.actions_e.clone() } else { head.actions_h.clone() };
					new_actions.push(Open(loc));
					let mut new_actions = Some(new_actions);

					let head = Head {
						node, time, pressure_released, valves_opened,
						actions_h: if is_e { head.actions_h.clone() } else { replace(&mut new_actions, None).unwrap() },
						actions_e: if is_e { new_actions.unwrap() } else { head.actions_e.clone() },
					};
					new_heads.push(head);
				}
				for move_to in &valve.tunnels_to {
					let mut node = head.node;
					if is_e { node.valve_e = move_to; } else { node.valve_h = move_to; }

					let mut new_actions = if is_e { head.actions_e.clone() } else { head.actions_h.clone() };
					new_actions.push(Move(move_to));
					let mut new_actions = Some(new_actions);

					let head = Head {
						node, time, pressure_released, valves_opened: head.valves_opened.clone(),
						actions_h: if is_e { head.actions_h.clone() } else { replace(&mut new_actions, None).unwrap() },
						actions_e: if is_e { new_actions.unwrap() } else { head.actions_e.clone() },
					};
					new_heads.push(head);
				}
				new_heads
			};

			let mut pushed_head = false;
			for head in gen_new_heads(&head, false) {
				for head in gen_new_heads(&head, true) {
					let prev_time = nodes.get(&head.node).cloned().unwrap_or(usize::MAX);
					if head.time < prev_time {
						nodes.insert(head.node, head.time);
					}
					if head.time <= prev_time {
						heads.push(head);
						pushed_head = true;
					}
				}
			}
			if !pushed_head {
				let head = Head {
					node: head.node, time, pressure_released, valves_opened: head.valves_opened,
					actions_h: head.actions_h,
					actions_e: head.actions_e,
				};
				heads.push(head);
			}
		}
		heads.sort_by_key(|head| head.pressure_released);
		heads.reverse();
		let limit = 100000;
		if heads.len() > limit {
			heads.splice(limit.., None);
		}
		println!("Heads: {}", heads.len());
		println!("Best: {:?}", heads.first());
	}
}

#[derive(Debug, PartialEq, Clone)]
struct Valve<'a> {
	name: &'a str,
	flow_rate: usize,
	tunnels_to: Vec<&'a str>,
}

#[derive(Debug, PartialEq, Clone)]
enum Action<'a> {
	Open(&'a str),
	Move(&'a str),
}
use Action::*;
impl<'a> Action<'a> {
	fn loc(&self) -> &'a str {
		match self {
			Open(loc) => loc,
			Move(loc) => loc,
		}
	}
}
