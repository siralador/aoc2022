use std::path::PathBuf;
use std::mem::replace;
use std::io::{Read, Write};
use std::fs::File;
use std::collections::{VecDeque, HashSet, HashMap};
use regex::{Regex, Match};
use anyhow::{Result, Context, anyhow};

mod utils;
use utils::*;

fn main() {
	let input: String = {
		let input_path = std::env::args().nth(1).unwrap_or("input.txt".to_string());
		let mut input_file = File::open(input_path).expect("Failed to open the input file");
		let mut input = String::new();
		input_file.read_to_string(&mut input).expect("Failed to read the input");
		input = input.trim().to_string();
		input
	};

	let lines = input.lines().collect::<Vec<_>>();

	let mut x = 1isize;
	let mut cycle = 0usize;
	let important = vec![20usize, 60, 100, 140, 180, 220];
	let mut sum = 0;
	macro_rules! cycle {
		() => { {
			cycle += 1;
			if important.contains(&cycle)  {
				let signal = cycle as isize * x;
				sum += signal;
				println!("X: {}. Cycle: {}. Signal: {}", x, cycle, signal);
			}
		} }
	}

	for line in &lines {
		let bits = line.split(" ").vec();
		match bits[0] {
			"addx" => {
				let addr = bits[1].parse::<isize>().expect("Failed to parse addx");
				cycle!();
				cycle!();
				x += addr;
			},
			"noop" => {
				cycle!();
			},
			_ => panic!("Unknown instruction {}", bits[0]),
		}

	}

	println!("Sum: {}", sum);

	main2(&lines);
}

fn main2(lines: &Vec<&str>) {
	let mut x = 1isize;
	let mut cycle = 0usize;
	let important = vec![20usize, 60, 100, 140, 180, 220];
	let mut sum = 0;
	macro_rules! cycle {
		() => { {
			if cycle % 40 == 0 { println!(""); }
			if (((cycle as isize % 40)) - x).abs() <= 1 { print!("#"); }
			else { print!("."); }
			cycle += 1;
		/*	if important.contains(&cycle)  {
				let signal = cycle as isize * x;
				sum += signal;
				println!("X: {}. Cycle: {}. Signal: {}", x, cycle, signal);
			}*/
		} }
	}

	for line in lines {
		let bits = line.split(" ").vec();
		match bits[0] {
			"addx" => {
				let addr = bits[1].parse::<isize>().expect("Failed to parse addx");
				cycle!();
				cycle!();
				x += addr;
			},
			"noop" => {
				cycle!();
			},
			_ => panic!("Unknown instruction {}", bits[0]),
		}

	}

	println!("");
	//println!("Sum: {}", sum);

}
