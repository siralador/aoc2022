use std::io::{Read, Write};
use std::fs::File;

fn main() {
	let input: String = {
		let input_path = std::env::args().nth(1).unwrap_or("input.txt".to_string());
		let mut input_file = File::open(input_path).expect("Failed to open the input file");
		let mut input = String::new();
		input_file.read_to_string(&mut input).expect("Failed to read the input");
		input.trim().to_string()
	};

	let turns = input.lines()
		.map(|line| {
			let line = line.trim().to_lowercase();
			(Action::read(line.as_bytes()[0] as char), Action::read(line.as_bytes()[2] as char))
		})
		.collect::<Vec<_>>();
	
	let score: usize = turns.iter().cloned().map(|pair| do_score(pair)).sum();
	println!("{score}");

	let mapped = turns.into_iter().map(|(them, you)| (them, you.into_condition().should_be_action(them))).collect::<Vec<_>>();
	let score: usize = mapped.iter().cloned().map(|pair| do_score(pair)).sum();
	println!("{score}");
}

type ActionPair = (Action, Action);
fn do_score(action: (Action, Action)) -> usize {
	let item = action.1.score();
	let win = if (action.1.beats() == action.0) { 6 } else if (action.0.beats() == action.1) { 0 } else { 3 };
	return item + win;
}

#[derive(PartialEq, Eq, Debug, Clone, Copy)]
enum Action {
	Rock,
	Paper,
	Scissors
}
use Action::*;
impl Action {
	fn read(c: char) -> Action {
		match c {
			'a' => Rock,
			'b' => Paper,
			'c' => Scissors,
			'x' => Rock,
			'y' => Paper,
			'z' => Scissors,
			_ => panic!("Invalid instruction {}", c)
		}
	}

	fn score(&self) -> usize {
		match self {
			Rock => 1,
			Paper => 2,
			Scissors => 3
		}
	}

	fn beats(&self) -> Action {
		match self {
			Rock => Scissors,
			Paper => Rock,
			Scissors => Paper,
		}
	}

	fn into_condition(&self) -> Condition {
		match self {
			Rock => Loose,
			Paper => Draw,
			Scissors => Win
		}
	}
}


#[derive(PartialEq, Eq, Debug, Clone, Copy)]
enum Condition {
	Loose,
	Draw,
	Win
}
use Condition::*;
impl Condition {
	fn should_be_action(&self, against: Action) -> Action {
		match self{
			Loose => against.beats(),
			Draw => against,
			Win => match against {
				Rock => Paper,
				Paper => Scissors,
				Scissors => Rock,
			}
		}
	}
}
