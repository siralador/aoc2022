pub trait IteratorExt: Iterator {
	fn vec(self) -> Vec<Self::Item>;
}

impl<O, I: Iterator<Item = O>> IteratorExt for I {
	fn vec(self) -> Vec<Self::Item> { self.collect() }
}
