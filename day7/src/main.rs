use std::path::PathBuf;
use std::io::{Read, Write};
use std::fs::File;
use std::collections::{VecDeque, HashSet, HashMap};
use regex::{Regex, Match};

mod utils;
use utils::*;

fn main() {
	let input: String = {
		let input_path = std::env::args().nth(1).unwrap_or("input.txt".to_string());
		let mut input_file = File::open(input_path).expect("Failed to open the input file");
		let mut input = String::new();
		input_file.read_to_string(&mut input).expect("Failed to read the input");
		input.trim().to_string()
	};

	let lines = input.lines().collect::<Vec<_>>();

	let mut current_dir = PathBuf::from("/");
	let mut sizes = HashMap::new();
	for line in lines {
		let bits = line.split(" ").vec();
		if bits[0] == "$" {
			match bits[1] {
				"cd" => {
					let path = PathBuf::from(bits[2]);
					let mut strr = path.as_os_str().to_str().unwrap();
					if strr.as_bytes()[0] == '/' as u8 {
						current_dir = PathBuf::from("/");
						strr = &strr[1..];
					}
					for pbit in strr.split("/") {
						if pbit == ".." {
							current_dir.pop();
						} else {
							current_dir.push(pbit);
						}
					}
					//current_dir = current_dir.absolute().expect("Unable to absolute");
				},
				"ls" =>  {},
				_ => panic!("unknown command {}", bits[1])
			}
		} else if bits[0] != "dir" {
			let add = bits[0].parse::<usize>().unwrap();
			let mut my_path_yes = Some(current_dir.clone());
			while let Some(my_path) = my_path_yes {
				let mut size = *sizes.get(&my_path).unwrap_or(&0);
				size += add;
				sizes.insert(my_path.clone(), size);
				my_path_yes = my_path.parent().map(|p| p.to_owned());
			}
		}
	}

	let valid = sizes.iter().filter(|(path, size)| **size <= 100000).vec();
	/*let delete = valid.iter().filter(|(path, _size)| {
		let mut include = true;
		for (other, _os) in &valid {
			if path == other { continue }
			if path.starts_with(other) {
				include = false;
				break;
			}
		}
		include
	}).cloned().vec();*/
	let sum: usize = valid.iter().map(|(_p, size)| **size).sum();
	println!("{}", sum);

	let root = PathBuf::from("/");
	let total = 70000000;
	let required = 30000000;
	let free_space = total - sizes[&root];
	let could_depete = sizes.iter().filter(|(path, size)| free_space + **size >= required).vec();
	println!("{:?}", sizes);
	println!("{:?}", could_depete);
	let (path, cheapest) = could_depete.iter().min_by_key(|(_, size)| **size).unwrap();
	println!("{}", cheapest);
	println!("{:?}", path);
}
