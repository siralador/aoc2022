use std::path::PathBuf;
use std::ops::{Deref, DerefMut};
use std::mem::replace;
use std::io::{Read, Write};
use std::fs::File;
use std::collections::{BTreeMap, VecDeque, HashSet, HashMap};
use regex::{Regex, Match};
use anyhow::{Result, Context, anyhow};
use std::str::Chars;
use std::iter::Peekable;
use std::cmp::Ordering;
use rayon::prelude::*;
use std::collections::BTreeSet;

mod utils;
use utils::*;

const ARENA_WIDTH: usize = 7;

fn main() {
	let input: String = {
		let input_path = std::env::args().nth(1).unwrap_or("input.txt".to_string());
		let mut input_file = File::open(input_path).expect("Failed to open the input file");
		let mut input = String::new();
		input_file.read_to_string(&mut input).expect("Failed to read the input");
		//input = input.trim().to_string();
		input
	};

	let lines = input.lines().collect::<Vec<_>>();

	let board_lines = lines.iter().take_while(|line| !line.is_empty()).vec();
	let instruction_line = lines.iter().skip(board_lines.len() + 1).next().unwrap();

	let mut board: BTreeMap<GridLoc, Tile> = BTreeMap::new();
	let max_y = board_lines.len();
	let mut max_x = 1;
	for (i, line) in board_lines.iter().enumerate() {
		let y = i + 1;
		for (i, c) in line.chars().enumerate() {
			let x = i + 1;
			max_x = max_x.max(x);
			let tile = match c {
				'.' => Some(Ground),
				'#' => Some(Wall),
				_ => None
			};
			if let Some(tile) = tile { board.insert((x, y), tile); }
		}
	}
	let max_x = max_x;
	let board = board;

	let mut instruction_iter = instruction_line.chars().peekable();
	let mut instructions = vec![];
	while let Some(c) = instruction_iter.next() {
		if c.is_digit(10) {
			let mut num = String::new();
			num.push(c);
			while let Some(c) = instruction_iter.peek() {
				if c.is_digit(10) { num.push(instruction_iter.next().unwrap()); }
				else { break; }
			}
			instructions.push(Forward(num.parse().unwrap()));
		}
		else if c == 'L' { instructions.push(TurnLeft); }
		else if c == 'R' { instructions.push(TurnRight); }
	}

	let enforce_pos = move |mut pos: GridLoc| {
		if pos.0 == 0 { pos.0 = max_x; }
		if pos.1 == 0 { pos.1 = max_y; }
		if pos.0 > max_x { pos.0 = 1; }
		if pos.1 > max_y { pos.1 = 1; }
		pos
	};

	let begin = board.keys().cloned().reduce(|(ax, ay), (bx, by)| if ay < by || (ay == by && ax < bx) { (ax, ay) } else { (bx, by) }).unwrap();
	let mut pos = begin;
	let mut facing = FRight;
	for (i, instruction) in instructions.iter().enumerate() {
		println!("Instruction {}", i);
		match instruction {
			Forward(amount) => {
				let mut new_pos = pos;
				'moving: for _ in 0..*amount {
					new_pos = enforce_pos(facing.apply(new_pos));
					while board.get(&new_pos).is_none() {
						new_pos = enforce_pos(facing.apply(new_pos));
					}
					if board.get(&new_pos) == Some(&Wall) { break 'moving; }
					assert!(board.get(&new_pos) == Some(&Ground));
					pos = new_pos;
				}
			},
			TurnLeft => { facing = facing.turn_left(); },
			TurnRight => { facing = facing.turn_right(); },
		}
		//print_state(&board, pos, facing, max_x, max_y);
	}
	println!("{:?} {:?}", pos, facing);
	println!("{}", (1000 * pos.1) + (4 * pos.0) + facing.num());

	let section_size = if max_x > 50 { 50 } else { 4 };
	println!("Section size: {}", section_size);
	let segsiz = section_size;
	let mut sections:  BTreeMap<CubeFace, GridBuffer<Tile>> = BTreeMap::new();
	let mut section_unmap: BTreeMap<CubeFace, (GridLoc, Rotation)> = BTreeMap::new();
	fn deduce_sections(board: &BTreeMap<GridLoc, Tile>, sections: &mut BTreeMap<CubeFace, GridBuffer<Tile>>, section: GridLoc, segsiz: usize, my_face: CubeFace, rotation: Rotation, section_unmap: &mut BTreeMap<CubeFace, (GridLoc, Rotation)>) {
		let origin = (section.0 * segsiz + 1, section.1 * segsiz + 1);
		println!("{:?} {:?} {:?} {:?}", section, origin, my_face, rotation);
		assert!(board.get(&origin).is_some());
		sections.insert(
			my_face,
			GridBuffer::from_raw(
				segsiz,
				segsiz,
				(0..segsiz)
				.map(
					|y| (0..segsiz)
						.map(move |x| (x, y))
						.map(|loc| (R0 - rotation).rotate(loc, segsiz))
				)
				.flatten()
				.map(|(x, y)| (x + origin.0, y + origin.1))
				.map(|loc| board[&loc])
				.vec()
			)
		);
		section_unmap.insert(my_face, (section, rotation));

		let mut faces: VecDeque<_> = vec![my_face.to_up(), my_face.to_right(), my_face.to_down(), my_face.to_left()].into_iter().collect();
		//println!("{:?} {:?}", R0 - rotation, (R0 -rotation).num());
		println!("{:?} {:?}", rotation, rotation.num());
		for _ in 0..(rotation).num() {
			let face = faces.pop_front().unwrap();
			faces.push_back(face);
		}
		//let to = my_face.to_up();
		let to = faces.pop_front().unwrap();
		if !sections.contains_key(&to) {
			if let Some(y) = section.1.checked_sub(1) {
				let section = (section.0, y);
				let new_origin = (section.0 * segsiz + 1, section.1 * segsiz + 1);
				if board.get(&new_origin).is_some() {
					deduce_sections(board, sections, section, segsiz, to, rotation - Rotation::calc(my_face, to), section_unmap);
				}
			}
		}
		//let to = my_face.to_right();
		let to = faces.pop_front().unwrap();
		if !sections.contains_key(&my_face.to_right()) {
			let section = (section.0 + 1, section.1);
			let new_origin = (section.0 * segsiz + 1, section.1 * segsiz + 1);
			if board.get(&new_origin).is_some() {
				deduce_sections(board, sections, section, segsiz, to, rotation - Rotation::calc(my_face, to), section_unmap);
			}
		}
		//let to = my_face.to_down();
		let to = faces.pop_front().unwrap();
		if !sections.contains_key(&to) {
			let section = (section.0, section.1 + 1);
			let new_origin = (section.0 * segsiz + 1, section.1 * segsiz + 1);
			if board.get(&new_origin).is_some() {
				deduce_sections(board, sections, section, segsiz, to, rotation - Rotation::calc(my_face, to), section_unmap);
			}
		}
		//let to = my_face.to_left();
		let to = faces.pop_front().unwrap();
		if !sections.contains_key(&to) {
			if let Some(x) = section.0.checked_sub(1) {
				let section = (x, section.1);
				let new_origin = (section.0 * segsiz + 1, section.1 * segsiz + 1);
				if board.get(&new_origin).is_some() {
					deduce_sections(board, sections, section, segsiz, to, rotation - Rotation::calc(my_face, to), section_unmap);
				}
			}
		}
	}
	deduce_sections(&board, &mut sections, (begin.0 / section_size, begin.1 / section_size), section_size, Top, R0, &mut section_unmap);

	/*println!("made it {:?}", sections.keys().vec());
	for (face, dat) in &sections {
		println!("{:?}: {:?}", face, dat.grid().row(0).unwrap().into_iter().map(|(a, b)| b.str()).vec());
	}*/

	let unmap = |pos: GridLoc, facing: Facing, on: CubeFace| {
		let (section, already_applied_rotation) = section_unmap[&on];
		let inv_rotation = R0 - already_applied_rotation;
		let local = (pos.0 - 1, pos.1 - 1);
		let local = inv_rotation.rotate(local, segsiz);
		let origin = (section.0 * segsiz + 1, section.1 * segsiz + 1);
		let pos = (origin.0 + local.0, origin.1 + local.1);
		(pos, facing.rotate(&inv_rotation))
	};

	let cube = sections;

	let mut pos = (1, 1);
	let mut on = Top;
	let mut facing = FRight;
	for (i, instruction) in instructions.iter().enumerate() {
		println!("Instruction {}: {:?}", i, instruction);
		match instruction {
			Forward(amount) => {
				let mut new_pos = pos;
				let mut new_on = on;
				let mut new_facing = facing;
				'moving: for _ in 0..*amount {
					let mut advance = || {
						new_pos = new_facing.apply(new_pos);
						if new_pos.0 == 0 {
							let new_new_on = new_on.to_left();
							let rotation = R0 - Rotation::calc(new_on, new_new_on);
							let local = (segsiz - 1, pos.1 - 1);
							let local = rotation.rotate(local, segsiz);
							new_pos = (local.0 + 1, local.1 + 1);
							new_facing = new_facing.rotate(&rotation);
							new_on = new_new_on;
						}
						else if new_pos.1 == 0 {
							let new_new_on = new_on.to_up();
							let rotation = R0 - Rotation::calc(new_on, new_new_on);
							let local = (pos.0 - 1, segsiz - 1);
							let local = rotation.rotate(local, segsiz);
							new_pos = (local.0 + 1, local.1 + 1);
							new_facing = new_facing.rotate(&rotation);
							new_on = new_new_on;
						}
						else if new_pos.0 > segsiz {
							let new_new_on = new_on.to_right();
							let rotation = R0 - Rotation::calc(new_on, new_new_on);
							let local = (0, pos.1 - 1);
							let local = rotation.rotate(local, segsiz);
							new_pos = (local.0 + 1, local.1 + 1);
							new_facing = new_facing.rotate(&rotation);
							new_on = new_new_on;
						}
						else if new_pos.1 > segsiz {
							let new_new_on = new_on.to_down();
							let rotation = R0 - Rotation::calc(new_on, new_new_on);
							let local = (pos.0 - 1, 0);
							let local = rotation.rotate(local, segsiz);
							new_pos = (local.0 + 1, local.1 + 1);
							new_facing = new_facing.rotate(&rotation);
							new_on = new_new_on;
						}
					};
					advance();
					let grid = cube[&new_on].grid();
					let tile = grid.get((new_pos.0 - 1, new_pos.1 - 1));
					println!("{:?} {:?} {:?}", new_pos, new_facing, new_on);
					if matches!(tile, Ok(&Wall)) {
						println!("break;");
						break 'moving;
					}
					assert!(matches!(tile, Ok(&Ground)));
					pos = new_pos;
					facing = new_facing;
					on = new_on;
					//let (unmap_pos, unmap_facing) = unmap(pos, facing, on);
					//print_state(&board, unmap_pos, unmap_facing, max_x, max_y);
				}
			},
			TurnLeft => { facing = facing.turn_left(); },
			TurnRight => { facing = facing.turn_right(); },
		}
		//let (unmap_pos, unmap_facing) = unmap(pos, facing, on);
		//print_state(&board, unmap_pos, unmap_facing, max_x, max_y);
	}
	println!("{:?} {:?} {:?}", pos, facing, on);

	let (unmap_pos, unmap_facing) = unmap(pos, facing, on);
	println!("{:?} {:?}", unmap_pos, unmap_facing);
	println!("{}", (1000 * unmap_pos.1) + (4 * unmap_pos.0) + unmap_facing.num());
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
enum Tile {
	Ground,
	Wall
}
use Tile::*;
impl Tile {
	fn str(&self) -> &'static str {
		match self{
			Ground => ".",
			Wall => "#",
		}
	}
}

#[derive(Debug, Clone)]
enum Instruction {
	Forward(usize),
	TurnLeft,
	TurnRight,
}
use Instruction::*;

#[derive(Debug, Clone, Copy)]
enum Facing {
	FUp,
	FRight,
	FDown,
	FLeft
}
use Facing::*;
impl Facing {
	fn turn_left(&self) -> Facing {
		match self {
			FUp => FLeft,
			FRight => FUp,
			FDown => FRight,
			FLeft => FDown,
		}
	}
	fn turn_right(&self) -> Facing {
		match self {
			FUp => FRight,
			FRight => FDown,
			FDown => FLeft,
			FLeft => FUp,
		}
	}
	fn apply(&self, (x, y): GridLoc) -> GridLoc {
		match self {
			FUp => (x, y-1),
			FRight => (x+1, y),
			FDown => (x, y+1),
			FLeft => (x-1, y),
		}
	}
	fn num(&self) -> usize {
		match self {
			FRight => 0,
			FDown => 1,
			FLeft => 2,
			FUp => 3,
		}
	}
	fn str(&self) -> &'static str {
		match self {
			FRight => ">",
			FDown => "_",
			FLeft => "<",
			FUp => "T",
		}
	}
	fn rotate(&self, rotation: &Rotation) -> Facing {
		match (self, rotation) {
			(FUp, R0) => FUp,
			(FUp, R90) => FRight,
			(FUp, R180) => FDown,
			(FUp, R270) => FLeft,
			(FRight, R0) => FRight,
			(FRight, R90) => FDown,
			(FRight, R180) => FLeft,
			(FRight, R270) => FUp,
			(FDown, R0) => FDown,
			(FDown, R90) => FLeft,
			(FDown, R180) => FUp,
			(FDown, R270) => FRight,
			(FLeft, R0) => FLeft,
			(FLeft, R90) => FUp,
			(FLeft, R180) => FRight,
			(FLeft, R270) => FDown,
		}
	}
}

fn print_state(board: &BTreeMap<GridLoc, Tile>, pos: GridLoc, facing: Facing, max_x: usize, max_y: usize) {
	for y in 1..=max_y {
		for x in 1..=max_x {
			if (x, y) == pos {
				print!("{}", facing.str());
			} else {
				if let Some(tile) = board.get(&(x, y)) { print!("{}", tile.str()); }
				else { print!(" "); }
			}
		}
		println!("");
	}
	println!("\n\n");
}

//clockwise
#[derive(Debug, Clone, Copy)]
enum Rotation {
	R0,
	R90,
	R180,
	R270,
}
use Rotation::*;
impl Rotation {
	fn rotate(&self, loc: GridLoc, segsiz: usize) -> GridLoc {
		match self {
			R0 => loc,
			R90 => (segsiz - loc.1 - 1, loc.0),
			R180 => (segsiz - loc.0 - 1, segsiz - loc.1 - 1),
			R270 => (loc.1, segsiz - loc.0 - 1),
		}
	}
	fn rot_right(&self) -> Rotation {
		match self{
			R0 => R90,
			R90 => R180,
			R180 => R270,
			R270 => R0,
		}
	}
	fn rot_left(&self) -> Rotation {
		match self{
			R180 => R90,
			R270 => R180,
			R0 => R270,
			R90 => R0,
		}
	}
	fn calc(from: CubeFace, to: CubeFace) -> Rotation {
		let identity = || panic!("Identity {:?} {:?}", from, to);
		let no = || panic!("No {:?} {:?}", from, to);
		match (from, to) {
			(Top, Top) => identity(),
			(Top, Up) => R180,
			(Top, Down) => R0,
			(Top, Left) => R90,
			(Top, Right) => R270,
			(Top, Bottom) => no(),
			(Down, Down) => identity(),
			(Down, Top) => R0,
			(Down, Bottom) => R0,
			(Down, Left) => R0,
			(Down, Right) => R0,
			(Down, Up) => no(),
			(Left, Left) => identity(),
			(Left, Top) => R270,
			(Left, Bottom) => R90,
			(Left, Up) => R0,
			(Left, Down) => R0,
			(Left, Right) => no(),
			(Right, Right) => identity(),
			(Right, Top) => R90,
			(Right, Bottom) => R270,
			(Right, Down) => R0,
			(Right, Up) => R0,
			(Right, Left) => no(),
			(Up, Up) => identity(),
			(Up, Top) => R180,
			(Up, Bottom) => R180,
			(Up, Right) => R0,
			(Up, Left) => R0,
			(Up, Down) => no(),
			(Bottom, Bottom) => identity(),
			(Bottom, Down) => R0,
			(Bottom, Up) => R180,
			(Bottom, Left) => R270,
			(Bottom, Right) => R90,
			(Bottom, Top) => no(),
		}
	}
	fn num(&self) -> u8 {
		match self {
			R0 => 0,
			R90 => 1,
			R180 => 2,
			R270 => 3,
		}
	}
	fn from_num(num: u8) -> Self {
		match num {
			0 => R0,
			1 => R90,
			2 => R180,
			3 => R270,
			_ => panic!("{num}"),
		}
	}
}
impl std::ops::Add<Rotation> for Rotation {
	type Output = Rotation;
	fn add(self, rhs: Rotation) -> Self::Output { Rotation::from_num((self.num() + rhs.num()) % 4) }
}
impl std::ops::Sub<Rotation> for Rotation {
	type Output = Rotation;
	fn sub(self, rhs: Rotation) -> Self::Output { Rotation::from_num((self.num() + 4 - rhs.num()) % 4) }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
enum CubeFace {
	Top,
	Bottom,
	Up,
	Down,
	Left,
	Right
}
use CubeFace::*;
impl CubeFace {
	fn to_up(&self) -> CubeFace {
		match self {
			Top => Up,
			Bottom => Down,
			Up => Top,
			Down => Top,
			Left => Top,
			Right => Top,
		}
	}
	fn to_down(&self) -> CubeFace {
		match self {
			Top => Down,
			Bottom => Up,
			Up => Bottom,
			Down => Bottom,
			Left => Bottom,
			Right => Bottom,
		}
	}
	fn to_left(&self) -> CubeFace {
		match self {
			Top => Left,
			Bottom => Left,
			Down => Left,
			Up => Right,
			Left => Up,
			Right => Down,
		}
	}
	fn to_right(&self) -> CubeFace {
		match self{
			Top => Right,
			Bottom => Right,
			Down => Right,
			Up => Left,
			Left => Down,
			Right => Up,
		}
	}
}
