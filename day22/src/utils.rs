

pub trait IteratorExt: Iterator {
	fn vec(self) -> Vec<Self::Item>;
}

impl<O, I: Iterator<Item = O>> IteratorExt for I {
	fn vec(self) -> Vec<Self::Item> { self.collect() }
}

use std::collections::BTreeMap;
pub trait IteratorToMapExt: Iterator {
	type Map;
	fn to_map(self) -> Self::Map;
}
impl<K: Ord, V, I: Iterator<Item = (K, V)>> IteratorToMapExt for I {
	type Map = BTreeMap<K, V>;
	fn to_map(self) -> Self::Map { self.collect() }
}


unsafe fn lifetime_override_mut<'a, 'b, T>(ptr: &'a mut T) -> &'b mut T { std::mem::transmute(ptr) }

mod grid;
pub use grid::*;

pub trait MyAsRef<T> {
	fn as_ref(&self) -> &T;
}
pub trait MyAsMut<T>: MyAsRef<T> {
	fn as_mut(&mut self) -> &mut T;
}
impl<T> MyAsRef<T> for &T {
	fn as_ref(&self) -> &T { self }
}
impl<T> MyAsRef<T> for &mut T {
	fn as_ref(&self) -> &T { self }
}
impl<T> MyAsMut<T> for &mut T {
	fn as_mut(&mut self) -> &mut T { self }
}


#[derive(Copy, Clone)]
pub struct PtrEq<'a, T>(pub &'a T);
impl<'a, T> PartialEq for PtrEq<'a, T> {
	fn eq(&self, other: &Self) -> bool { std::ptr::eq(self.0, other.0) }
}
impl<'a, T> Eq for PtrEq<'a, T> {}
impl<'a, T: std::fmt::Debug> std::fmt::Debug for PtrEq<'a, T> {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result { self.0.fmt(f) }
}
impl<'a, T: std::fmt::Display> std::fmt::Display for PtrEq<'a, T> {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result { self.0.fmt(f) }
}
impl<'a, T> std::hash::Hash for PtrEq<'a, T> {
	fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
		(self.0 as *const T as usize).hash(state)
	}
}
