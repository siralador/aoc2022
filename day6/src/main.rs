use std::io::{Read, Write};
use std::fs::File;
use std::collections::{VecDeque, HashSet};
use regex::{Regex, Match};

mod utils;
use utils::*;

fn main() {
	let input: String = {
		let input_path = std::env::args().nth(1).unwrap_or("input.txt".to_string());
		let mut input_file = File::open(input_path).expect("Failed to open the input file");
		let mut input = String::new();
		input_file.read_to_string(&mut input).expect("Failed to read the input");
		input.trim().to_string()
	};

	let lines = input.lines().collect::<Vec<_>>();

	let mut frame = VecDeque::new();
	for (i, c) in input.chars().enumerate() {
		frame.push_back(c);
		if frame.len() > 4 { frame.pop_front(); }
		let different: HashSet<char> = frame.clone().into_iter().collect();
		if different.len() >= 4 {
			println!("{}", i + 1);
			break;
		}
	}

	let mut frame = VecDeque::new();
	for (i, c) in input.chars().enumerate() {
		frame.push_back(c);
		if frame.len() > 14 { frame.pop_front(); }
		let different: HashSet<char> = frame.clone().into_iter().collect();
		if different.len() >= 14 {
			println!("{}", i + 1);
			break;
		}
	}
}
