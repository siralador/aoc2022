use std::path::PathBuf;
use std::mem::replace;
use std::io::{Read, Write};
use std::fs::File;
use std::collections::{BTreeMap, VecDeque, HashSet, HashMap};
use regex::{Regex, Match};
use anyhow::{Result, Context, anyhow};
use std::str::Chars;
use std::iter::Peekable;
use std::cmp::Ordering;
use rayon::prelude::*;
use std::collections::BTreeSet;

mod utils;
use utils::*;

const ARENA_WIDTH: usize = 7;

fn main() {
	let input: String = {
		let input_path = std::env::args().nth(1).unwrap_or("input.txt".to_string());
		let mut input_file = File::open(input_path).expect("Failed to open the input file");
		let mut input = String::new();
		input_file.read_to_string(&mut input).expect("Failed to read the input");
		input = input.trim().to_string();
		input
	};

	//let lines = input.lines().collect::<Vec<_>>();
	
	let rocks = [
		GridBuffer::from_raw(4, 1, vec![true, true, true, true]),
		GridBuffer::from_raw(3, 3, vec![
			false, true, false,
			true, true, true,
			false, true, false
		]),
		GridBuffer::from_raw(3, 3, vec![
			false, false, true,
			false, false, true,
			true, true, true,
		]),
		GridBuffer::from_raw(1, 4, vec![true, true, true, true]),
		GridBuffer::from_raw(2, 2, vec![true, true, true, true]),
	];

	let actions = input.trim().chars().map(|c| match c {
		'<' => PushToLeft,
		'>' => PushToRight,
		_ => panic!("What {}", c),
	}).vec();
	println!("{:?}", actions);

	do_rocks(&rocks, &actions, 2022);
	do_rocks(&rocks, &actions, 1000000000000);
}

fn do_rocks(rocks: &[GridBuffer<bool>; 5], actions: &Vec<JetAction>, limit: usize) {

	let mut area: BTreeSet<GridLoc> = BTreeSet::new();
	let mut ceiling = 0;

	fn rock_mapping<'a>(r: &'a GridBuffer<bool>) -> Grid<bool, &'a GridBuffer<bool>> { r.grid() }

	let mut rock_i = 0;

	let mut past_states: HashMap<State, StateStatus> = HashMap::new();
	let mut has_repeated = false;
	let mut action_i = 0;
	while rock_i < limit {
		let doing_rock_i = rock_i % rocks.len();
		let rock = rocks[doing_rock_i].grid();

		let mut rock_pos: GridLoc = (2, ceiling + 3 + rock.height());

		fn test_rock(rock_pos: GridLoc, rock: &Grid<bool, &GridBuffer<bool>>, area: &BTreeSet<GridLoc>) -> bool {
			let mut is_valid = true;
			for (local_pos, is_rock) in rock.iter() {
				if *is_rock {
					let test_pos = rock_local_to_area(rock_pos, local_pos);
					if area.contains(&test_pos) {
						is_valid = false;
						break;
					}
				}
			}
			is_valid
		}

		let mut action;
		loop {
			action = actions[action_i];
			action_i = (action_i + 1) % actions.len();

			//println!("{:?}", action);
			let new_rock_pos = match action {
				PushToLeft => rock_pos.0.checked_sub(1).map(|x| (x, rock_pos.1)),
				PushToRight => Some((rock_pos.0 + 1, rock_pos.1)),
			};
			if let Some(new_rock_pos) = new_rock_pos {
				if new_rock_pos.0 + rock.width() <= ARENA_WIDTH {
					if test_rock(new_rock_pos, &rock, &area) {
						rock_pos = new_rock_pos;
					}
				}
			}

			if rock_i < 15 {
				print_area(&area, ceiling + 10, Some((rock_pos, &rock)));
			}

			let new_rock_pos = (rock_pos.0, rock_pos.1 - 1);
			if new_rock_pos.1 + 1 - rock.height() <= 0 { break; }
			if test_rock(new_rock_pos, &rock, &area) {
				rock_pos = new_rock_pos;
			} else {
				break;
			}

			if rock_i < 15 {
				print_area(&area, ceiling + 10, Some((rock_pos, &rock)));
			}
		}

		for (local_pos, is_rock) in rock.iter() {
			if *is_rock {
				area.insert(rock_local_to_area(rock_pos, local_pos));
			}
		}
		ceiling = ceiling.max(rock_pos.1);

		if rock_i < 50 {
			print_area(&area, ceiling, None);
		}

		let state = State {
			topology: do_topology(&mut area),
			just_did_rock_i: doing_rock_i,
			just_did_action_i: action_i,
		};
		if rock_i < 50 {
			print_area(&area, ceiling, None);
			println!("\n\n\n\n===============");
		}
		if let (Some(past_status), false) = (past_states.get(&state), has_repeated) {
			println!("Found repeating state");
			println!("{:?}", state);
			let current_status = StateStatus { rock_i, ceiling };
			println!("{:?} -> {:?}", past_status, current_status);

			let fast_forward_increment = current_status.rock_i - past_status.rock_i;
			let jump_increment = current_status.ceiling - past_status.ceiling;
			println!("(({limit} - {rock_i}) / {fast_forward_increment}) - 1");
			if let Some(fast_forward_quantity) = ((limit - rock_i) / fast_forward_increment).checked_sub(1) { //Don't do the last fast-forward for reasons
				println!("Fast forwarding {} times ({} * {} = {})", fast_forward_quantity, fast_forward_increment, fast_forward_quantity, fast_forward_quantity * fast_forward_increment);
				rock_i += fast_forward_increment * fast_forward_quantity;
				println!("New rock_i: {}", rock_i);
				println!("Jump increment: {jump_increment}");
				let jump = jump_increment * fast_forward_quantity;
				println!("Jumped {} rows", jump);

				area = area.into_iter().map(|loc| (loc.0, loc.1 + jump)).collect();
				ceiling += jump;
				println!("New ceiling: {}", ceiling);
			} else {
				println!("Skipping fast-forward");
			}

			has_repeated = true;
		} else {
			past_states.insert(state, StateStatus {
				rock_i, ceiling
			});
		}
		rock_i += 1;
	}
	//print_area(&area, ceiling, None);
	println!("Ceiling: {}", ceiling);
}

fn rock_local_to_area(rock_pos: GridLoc, local_pos: GridLoc) -> GridLoc {
	(rock_pos.0 + local_pos.0, rock_pos.1 - local_pos.1)
}

fn print_area(area: &BTreeSet<GridLoc>, ceiling: usize, rock: Option<(GridLoc, &Grid<bool, &GridBuffer<bool>>)>) {
	let rock: BTreeSet<GridLoc> = rock.map( |(rock_pos, rock)|  {
		rock.iter()
			.filter(|(_, is_rock)| **is_rock)
			.map(|(pos, _)| rock_local_to_area(rock_pos, pos))
			.collect()
	}).unwrap_or_default();
	for y in (1..=ceiling).rev() {
		print!("O");
		for x in 0..ARENA_WIDTH {
			let loc = (x, y);
			let area_contains = area.contains(&loc);
			let rock_contains = rock.contains(&loc);
			if area_contains && rock_contains { print!("X"); }
			else if area_contains { print!("#"); }
			else if rock_contains { print!("@"); }
			else { print!("."); }
		}
		println!("O");
	}
	for x in 0..ARENA_WIDTH {
		print!("O");
	}
	println!("");
	println!("");
	println!("");
}

fn do_topology(area: &mut BTreeSet<GridLoc>) -> BTreeSet<GridLoc> {
	let mut topology = (0..ARENA_WIDTH).map(|_| 0).vec();
	for value in area.iter() {
		topology[value.0] = topology[value.0].max(value.1);
	}
	let min_topology = *topology.iter().min().unwrap();
	let min_topology = min_topology.checked_sub(1).unwrap_or(0);
	let max_topology = *topology.iter().max().unwrap();
	let max_topology = max_topology.checked_add(1).unwrap_or(max_topology);
	let mut topology = BTreeSet::new();
	for y in min_topology..=max_topology {
		for x in 0..ARENA_WIDTH {
			let loc = (x,y);
			if area.contains(&loc) { topology.insert(loc); }
		}
	}
	if let Some(min_y) = min_topology.checked_sub(100) {
		*area = replace(area, BTreeSet::new()).into_iter().filter(|(_x, y)| *y >= min_y).collect();
	}
	topology.into_iter().map(|(x, y)| (x, y - min_topology)).collect()
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
enum JetAction {
	PushToLeft,
	PushToRight,
}
use JetAction::*;

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
struct State {
	topology: BTreeSet<GridLoc>,
	just_did_rock_i: usize,
	just_did_action_i: usize,
}
#[derive(Debug, Clone)]
struct StateStatus {
	rock_i: usize,
	ceiling: usize,
}
