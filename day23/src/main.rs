use std::path::PathBuf;
use std::ops::{Deref, DerefMut};
use std::mem::replace;
use std::io::{Read, Write};
use std::fs::File;
use std::collections::{BTreeMap, VecDeque, HashSet, HashMap};
use regex::{Regex, Match};
use anyhow::{Result, Context, anyhow};
use std::str::Chars;
use std::iter::Peekable;
use std::cmp::Ordering;
use rayon::prelude::*;
use std::collections::BTreeSet;

mod utils;
use utils::*;

const ARENA_WIDTH: usize = 7;

type Pos = (isize, isize);

fn main() {
	let input: String = {
		let input_path = std::env::args().nth(1).unwrap_or("input.txt".to_string());
		let mut input_file = File::open(input_path).expect("Failed to open the input file");
		let mut input = String::new();
		input_file.read_to_string(&mut input).expect("Failed to read the input");
		input = input.trim().to_string();
		input
	};

	let lines = input.lines().collect::<Vec<_>>();

	let elf = '#';
	let mut elves: BTreeSet<Pos> = lines.iter()
		.enumerate()
		.map(|(y, line)| line.trim().chars()
			 .enumerate()
			 .filter(move |(_, c)| *c == elf)
			 .map(move |(x, _)| (x as isize, y as isize))
		)
		.flatten()
		.collect();

	let mut considerations = vec![North, South, West, East];

	for round in 1..=10 {
		do_round(&mut elves, &mut considerations);
		println!("ROund {round}");
	}
	let stats = Stats::calc(elves.iter().cloned());
	println!("{:?}", stats);

	for round in 11.. {
		if round % 100 == 0 { println!("Round {}", round); }
		if do_round(&mut elves, &mut considerations) {
			println!("Stable state in round {}. Last needed round {}", round, round - 1);
			break;
		}
	}
}

const EMPTY_CHECK: [Pos; 8] = [(-1, -1), (-1, 0), (-1, 1), (1, -1), (1, 0), (1, 1), (0, 1), (0, -1)];
fn add(a: Pos, b: Pos) -> Pos { (a.0 + b.0, a.1 + b.1) }
fn sub(a: Pos, b: Pos) -> Pos { (a.0 - b.0, a.1 - b.1) }

fn no_obstructing_elves(your_pos: Pos, mut considerations: impl Iterator<Item = Pos>, elves: &BTreeSet<Pos>) -> bool {
	considerations.find(|pos| elves.contains(&add(your_pos, *pos))).is_none()
}

fn do_round(elves: &mut BTreeSet<Pos>, considerations: &mut Vec<Consideration>) -> bool {
	let mut old_elves = elves.clone();
	let mut proposed_occupations: BTreeMap<Pos, usize> = BTreeMap::new();
	let proposed_moves = elves.par_iter().map(|elf| (elf.clone(), {
		//Empty check - do nothing
		if no_obstructing_elves(*elf, EMPTY_CHECK.iter().cloned(), elves) { None }
		else {
			//Do considerations
			considerations.iter()
				.find(|consideration| {
					let test = no_obstructing_elves(*elf, consideration.considerations().into_iter(), elves);
					//println!("{:?} + {:?} = {}", elf, consideration, test);
					test
				})
				.map(|consideration| {
					let target = consideration.move_to(*elf);
					target
				})
		}
	})).collect::<Vec<_>>();
	for (_, m) in &proposed_moves {
		if let Some(m) = m {
			if let Some(mm) = proposed_occupations.get_mut(m) {
				*mm += 1;
			} else {
				proposed_occupations.insert(*m, 1);
			}
		}
	}
	if proposed_moves.iter().find(|(_, proposition)| proposition.is_some()).is_none() { return true; }
	elves.clear();
	*elves = proposed_moves.par_iter().map(|(elf, proposition)| {
		let new_pos = if let Some(proposition) = proposition {
			let propositions_of_that_pos = proposed_occupations[&proposition];
			assert!(propositions_of_that_pos > 0);
			if propositions_of_that_pos == 1 { proposition }
			else { /*println!("Conflict at {:?} {}", proposition, propositions_of_that_pos);*/ elf }
		}
		else { elf };
		*new_pos
	})
		.collect::<Vec<_>>()
		.into_iter()
		.collect();

	/*let mut my_stats = Stats::calc(elves.iter().cloned());
	my_stats.min = sub(my_stats.min, (1, 1));
	my_stats.max = add(my_stats.max, (1, 1));
	//print_elves(&elves, &my_stats, Some(&old_elves));
	//println!("{:?}", considerations);
	*/

	let moving = considerations.remove(0);
	considerations.push(moving);

	return false;
}

#[derive(Debug, Clone)]
enum Consideration {
	North,
	South,
	West,
	East
}
use Consideration::*;
impl Consideration {
	fn considerations(&self) -> [Pos; 3] {
		match self {
			North => [(-1, -1), (0, -1), (1, -1)],
			South => [(-1, 1), (0, 1), (1, 1)],
			West => [(-1, -1), (-1, 0), (-1, 1)],
			East => [(1, -1), (1, 0), (1, 1)],
		}
	}
	fn move_to(&self, old_pos: Pos) -> Pos {
		add(old_pos, match self {
			North => (0, -1),
			South => (0, 1),
			West => (-1, 0),
			East => (1, 0)
		})
	}
}

#[derive(Debug)]
struct Stats {
	min: Pos,
	max: Pos,
	num_elves: usize,
	empty_tiles: usize
}
impl Stats {
	fn calc(elves: impl Iterator<Item = Pos>) -> Stats {
		let mut min = None;
		let mut max = None;
		let mut num_elves = 0;
		for elf in elves {
			min = Some(min.map(|min: Pos| (min.0.min(elf.0), min.1.min(elf.1))).unwrap_or(elf));
			max = Some(max.map(|max: Pos| (max.0.max(elf.0), max.1.max(elf.1))).unwrap_or(elf));
			num_elves += 1;
		};
		let min = min.unwrap();
		let max = max.unwrap();
		let dimensions = add(sub(max, min), (1, 1));
		Stats {
			min, max, num_elves,
			empty_tiles: (dimensions.0 * dimensions.1) as usize - num_elves
		}
	}
}

fn print_elves(elves: &BTreeSet<Pos>, stats: &Stats, old_elves: Option<&BTreeSet<Pos>>) {
	for y in stats.min.1..=stats.max.1 {
		for x in stats.min.0..=stats.max.0 {
			print!("{}",
				   if elves.contains(&(x, y)) { "#" }
				   else if old_elves.map(|old_elves| old_elves.contains(&(x, y))).unwrap_or(false) { "O" }
				   else { "." });
		}
		println!("");
	}
	println!("\n\n\n");
}
