use std::path::PathBuf;
use std::mem::replace;
use std::io::{Read, Write};
use std::fs::File;
use std::collections::{VecDeque, HashSet, HashMap};
use regex::{Regex, Match};
use anyhow::{Result, Context, anyhow};

mod utils;
use utils::*;

fn main() {
	let input: String = {
		let input_path = std::env::args().nth(1).unwrap_or("input.txt".to_string());
		let mut input_file = File::open(input_path).expect("Failed to open the input file");
		let mut input = String::new();
		input_file.read_to_string(&mut input).expect("Failed to read the input");
		input = input.trim().to_string();
		input
	};

	let lines = input.lines().collect::<Vec<_>>();

	let mut tail_positions = HashSet::<Loc>::new();
	let mut head = (0isize, 0isize);
	let mut tail = (0isize, 0isize);
	tail_positions.insert(tail);

	for action in lines.clone() {
		let bits = action.split(" ").vec(); 
		for _ in 0usize..bits[1].parse().unwrap() {
			match bits[0] {
				"L" => { head.0 -= 1; }
				"U" => { head.1 -= 1; },
				"R" => { head.0 += 1; },
				"D" => { head.1 += 1; },
				_ => panic!("Unknown cmd"),
			}
			update_tail(head, &mut tail, bits[0]);
			tail_positions.insert(tail);
		}
	}

	println!("{}", tail_positions.len());

	let mut tail_positions = HashSet::<Loc>::new();
	let mut nodes: Vec<Loc> = vec![(0, 0); 10];
	let mut actions: VecDeque<&str> = vec!["#"; 10].into_iter().collect();
	for action in lines {
		let bits = action.split(" ").vec(); 
		let quantity = bits[1].parse().unwrap();
		for i in 0usize..quantity {
			//println!("{}/{}", i, quantity);
			{
				let head = &mut nodes[0];
				match bits[0] {
					"L" => { head.0 -= 1; }
					"U" => { head.1 -= 1; },
					"R" => { head.0 += 1; },
					"D" => { head.1 += 1; },
					_ => panic!("Unknown cmd"),
				}
				actions.push_front(bits[0]);
				if actions.len() > nodes.len() { actions.pop_back(); }
			}
			for i in 1..nodes.len() {
				let head = nodes[i-1].clone();
				//debug(&nodes);
				let tail = &mut nodes[i];
				update_tail(head, tail, actions[i-1]);
			}
			//println!("");
			tail_positions.insert(nodes[nodes.len()-1]);
		}
	}

	debug_tail(&tail_positions);
	println!("{}", tail_positions.len());
}


type Loc = (isize, isize);

/*fn update_tail(head: Loc, tail: &mut Loc, cmd: &str) {
	while !((head.0 - tail.0).abs() <= 1 && (head.1 - tail.1).abs() <= 1) {
		let prev_tail = tail.clone();
		match cmd {
			"L" => { tail.0 -= 1; tail.1 = head.1; },
			"U" => { tail.1 -= 1; tail.0 = head.0; },
			"R" => { tail.0 += 1; tail.1 = head.1; },
			"D" => { tail.1 += 1; tail.0 = head.0; },
			"#" => {},
			_ => panic!("Unknown cmd"),
		}
		if *tail == head { panic!("Overlap {:?} {:?} via {}", head, prev_tail, cmd); }
	}
}*/
fn update_tail(head: Loc, tail: &mut Loc, cmd: &str) {
	while !((head.0 - tail.0).abs() <= 1 && (head.1 - tail.1).abs() <= 1) {
		let difx = (head.0 - tail.0).abs();
		let dify = (head.1 - tail.1).abs();
		if difx == dify {
			if head.0 > tail.0 { tail.0 = head.0 - 1; }
			else { tail.0 = head.0 + 1; }
			if head.1 > tail.1 { tail.1 = head.1 - 1; }
			else { tail.1 = head.1 + 1; }
		}
		else if difx > dify {
			if head.0 > tail.0 { tail.0 = head.0 - 1; }
			else { tail.0 = head.0 + 1; }
			tail.1 = head.1;
		} else {
			if head.1 > tail.1 { tail.1 = head.1 - 1; }
			else { tail.1 = head.1 + 1; }
			tail.0 = head.0;
		}
	}
}

fn debug(nodes: &Vec<Loc>) {
	for y in -20..20 {
		for x in -20..20 {
			if let Some((num, _)) = nodes.iter().enumerate().find(|(_, node)| **node == (x, y)) {
				print!("{}", num);
			}
			else { print!(".") }
		}
		println!("");
	}
		println!("");
			//std::thread::sleep(std::time::Duration::from_millis(100));
}
fn debug_tail(pos: &HashSet<Loc>) {
	let min_y = pos.iter().map(|(_, y)| *y).min().unwrap();
	let max_y = pos.iter().map(|(_, y)| *y).max().unwrap();
	let min_x = pos.iter().map(|(x, _)| *x).min().unwrap();
	let max_x = pos.iter().map(|(x, _)| *x).max().unwrap();
	for y in min_y..max_y {
		for x in min_x..max_x {
			if let Some((num, _)) = pos.iter().enumerate().find(|(_, node)| **node == (x, y)) {
				print!("#");
			}
			else { print!(".") }
		}
		println!("");
	}
		println!("");
			std::thread::sleep(std::time::Duration::from_millis(100));
}
