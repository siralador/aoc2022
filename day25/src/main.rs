use std::path::PathBuf;
use std::ops::{Deref, DerefMut};
use std::mem::replace;
use std::io::{Read, Write};
use std::fs::File;
use std::collections::{BTreeMap, VecDeque, HashSet, HashMap};
use regex::{Regex, Match};
use anyhow::{Result, Context, anyhow};
use std::str::Chars;
use std::iter::Peekable;
use std::cmp::Ordering;
use rayon::prelude::*;
use std::collections::BTreeSet;

mod utils;
use utils::*;

const ARENA_WIDTH: usize = 7;

fn main() {
	let input: String = {
		let input_path = std::env::args().nth(1).unwrap_or("input.txt".to_string());
		let mut input_file = File::open(input_path).expect("Failed to open the input file");
		let mut input = String::new();
		input_file.read_to_string(&mut input).expect("Failed to read the input");
		input = input.trim().to_string();
		input
	};

	let guide_input: String = {
		let input_path = "guide.txt".to_string();
		let mut input_file = File::open(input_path).expect("Failed to open the input file");
		let mut input = String::new();
		input_file.read_to_string(&mut input).expect("Failed to read the input");
		input = input.trim().to_string();
		input
	};
	for line in guide_input.lines() {
		let line = line.trim();
		let bits = line.split(" ").vec();
		let dec: isize = bits[0].parse().unwrap();
		let snafu = bits.last().unwrap();
		println!("Testing {dec} => {snafu}");
		let my_snafu = to_snafu(dec);
		println!("My snafu: {my_snafu}");
		assert!(*snafu == my_snafu.as_str());
	}

	let lines = input.lines().collect::<Vec<_>>();

	let nums: Vec<isize> = lines.iter().map(|line| {
		line.chars().rev().enumerate().map(|(i, c)| {
			(match c {
				'=' => -2,
				'-' => -1,
				'0' => 0,
				'1' => 1,
				'2' => 2,
				_ => panic!("What {}", c),
			}) * 5isize.pow(i as u32)
		}).sum()
	}).vec();

	let sum: isize = nums.iter().cloned().sum();

	println!("{sum}");
	println!("{}", to_snafu(sum));
}

fn to_snafu(mut num: isize) -> String {
	let original_num = num;
	let mut digits = (1..)
		.map(|i| 5isize.pow(i as u32))
		.take_while(|value| *value < num * 2)
		.vec();
	println!("{digits:?}");
	let mut digits = digits.into_iter()
		.rev()
		.map(|value| {
			let bit = num / value;
			num -= bit * value;
			bit
		})
		.vec();
	digits.reverse();
	digits.insert(0,num);
	digits.push(0);
	println!("{digits:?}");
	for i in 1..digits.len() {
		while digits[i-1] > 2 {
			digits[i-1] -= 5;
			digits[i] += 1;
		}
		while digits[i-1] < -2 { panic!("{:?} {i}", digits); }
	}
	println!("{digits:?}");
	let zero_count = digits.iter().rev().take_while(|d| **d == 0).count();
	digits.drain((digits.len()-zero_count)..);
	digits.into_iter().rev().map(|d| match d {
		-2 => '=',
		-1 => '-',
		0 => '0',
		1 => '1',
		2 => '2',
		_ => panic!("what {d}"),
	}).collect()
}

/*
struct SNAFU(Vec<i8>);
impl SNAFU {
	fn parse(s: &str) -> SNAFU {
		SNAFU(s.chars().rev().map(|c| match c {
			'=' => -2,
			'-' => -1,
			'0' => 0,
			'1' => 1,
			'2' => 2,
			_ => panic!("What {}", c),
		}).vec())
	}
}
impl std::fmt::Display for SNAFU {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		for n in &self.0 {
			f.write_str(match n {
				-2 => "=",
				-1 => "-",
				0 => "0",
				1 => "1",
				2 => "2",
				_ => panic!("What {n}"),
			})?;
		}
		Ok(())
	}
}
impl std::ops::Add for SNAFU {
	type Output = Self;
	fn add(self, rhs: Self) -> Self::Output {
		let mut buf = self.0.into_iter().zip(rhs.0.into_iter()).map(|(a, b)| a + b);
		let mut did_something = true;
		while did_something {
			did_something = false;
			for i in 0..buf.len() {
				while buf[i] > 2 {
					if i + 1 >= buf.len() { buf.push(len); }
					buf[i] -= 4 { 
			}
		}
	}
}
*/
