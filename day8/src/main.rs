use std::path::PathBuf;
use std::mem::replace;
use std::io::{Read, Write};
use std::fs::File;
use std::collections::{VecDeque, HashSet, HashMap};
use regex::{Regex, Match};
use anyhow::{Result, Context, anyhow};

mod utils;
use utils::*;

fn main() {
	let input: String = {
		let input_path = std::env::args().nth(1).unwrap_or("input.txt".to_string());
		let mut input_file = File::open(input_path).expect("Failed to open the input file");
		let mut input = String::new();
		input_file.read_to_string(&mut input).expect("Failed to read the input");
		input = input.trim().to_string();
		input
	};

	let lines = input.lines().collect::<Vec<_>>();
	
	let grid = lines.iter().map(|line| line.trim().chars().map(|c| c as u8 - b'0' as u8).vec()).vec();
	let mut visible = grid.iter().map(|row| row.iter().map(|_| Visibility::default()).vec()).vec();

	//Go along the top
	let width = grid[0].len();
	let height = grid[0].len();
	for x in 0..width {
		let mut previous = None;
		for y in 0..height {
			//print!("{:?} {} ", previous, grid[y][x] as i8);
			if previous.unwrap_or(-1i8) < grid[y][x] as i8 {
				visible[y][x].from_top = true;
				//print!("yes  ");
				previous = Some(grid[y][x] as i8);
			}
		}
		//println!("");
	}
	//Go along the bottom
	for x in 0..width {
		let mut previous = None;
		let mut ys = (0..height).vec();
		ys.reverse();
		for y in ys {
			if previous.unwrap_or(-1i8) < grid[y][x] as i8 {
				visible[y][x].from_bottom = true;
				previous = Some(grid[y][x] as i8);
			}
		}
	}
	//Go along the Right
	for y in 0..height {
		let mut previous = None;
		let mut xs = (0..width).vec();
		xs.reverse();
		for x in xs {
			if previous.unwrap_or(-1i8) < grid[y][x] as i8 {
				visible[y][x].from_right = true;
				previous = Some(grid[y][x] as i8);
			}
		}
	}
	//Go along the Left
	for y in 0..height {
		let mut previous = None;
		let mut xs = (0..width).vec();
		for x in xs {
			if previous.unwrap_or(-1i8) < grid[y][x] as i8 {
				visible[y][x].from_left = true;
				previous = Some(grid[y][x] as i8);
			}
		}
	}

	let num_visible = visible.clone().into_iter().flatten().filter(|tree| tree.visible()).count();
	println!("{}", num_visible);

	/*for y in 0..height {
		for x in 0..width {
			print!("{} {} ", grid[y][x], if visible[y][x].visible() { "." } else { "T" } );
		}
		println!("");
	}*/

	let field = grid.clone();
	let mut viewing = grid.iter().map(|row| row.iter().map(|_| Viewing::default()).vec()).vec();
	/*for x in 0..width {
		for y in 0..height {
			let me = field[y][x];
			if let Some(x) = x.checked_sub(1) {
				if field[y][x] >= me { viewing[y][x].from_right += 1; }
			}
			if let Some(y) = y.checked_sub(1) {
				if field[y][x] >= me { viewing[y][x].from_bottom += 1; }
			}
			{
				let x = x + 1;
				if x < width {
					if field[y][x] >= me { viewing[y][x].from_left += 1; }
				}
			}
			let y = y + 1;
			if y < height {
				if field[y][x] >= me { viewing[y][x].from_top += 1; }
			}
		}
	}
	let mut did_something = true;
	let mut iterations = 0;
	while did_something {
		iterations += 1;
		did_something = false;
		for x in 0..width {
			for y in 0..height {
				let me = field[y][x];
				let my_view = viewing[y][x].clone();
				let mut local_did_something = false;
				if my_view.has() {
					if let Some(x) = x.checked_sub(1) {
						if field[y][x] >= me { viewing[y][x].from_right += my_view.from_right; local_did_something = true; }
					}
					if let Some(y) = y.checked_sub(1) {
						if field[y][x] >= me { viewing[y][x].from_bottom += my_view.from_bottom; local_did_something = true; }
					}
					{
						let x = x + 1;
						if x < width {
							if field[y][x] >= me { viewing[y][x].from_left += my_view.from_left; local_did_something = true; }
						}
					}
					let y = y + 1;
					if y < height {
						if field[y][x] >= me { viewing[y][x].from_top += my_view.from_top; local_did_something = true; }
					}
				}
				if local_did_something {
					did_something = true;
					viewing[y][x] = Viewing::default();
				}
			}
		}
	}
	println!("{}", iterations);
	for row in &viewing {
		for v in row {
			print!("{:2} {:2} {:2} {:2}|", v.from_top, v.from_right, v.from_bottom, v.from_left);
		}
		println!("");
	}
	let max_view = viewing.into_iter().flatten().map(|v| v.score()).max();
	println!("{:?}", max_view);*/

	let mut max = 0;
	for x in 0..width {
		for y in 0..height {
			let mut left = 0;
			let mut up = 0;
			let mut right = 0;
			let mut down = 0;
			let me = field[y][x];
			{
				let mut x = x;
				while let Some(nx) = x.checked_sub(1) {
					x = nx;
					left += 1;
					if field[y][x] < me {  }
					else { break; }
				}
			}
			{
				let mut y = y;
				while let Some(ny) = y.checked_sub(1) {
					y = ny;
					up += 1;
					if field[y][x] < me {  }
					else { break; }
				}
			}
			{
				let mut x = x + 1;
				while x < width {
					right += 1;
					if field[y][x] < me {  }
					else { break; }
					x = x + 1;
				}
			}
			{
				let mut y = y + 1;
				while y < height {
					down += 1;
					if field[y][x] < me {  }
					else { break; }
					y = y + 1;
				}
			}
			let score = left * up * down * right;
			if score > max {
				println!("New score {}", score);
				max = score;
			}
		}
	}
}

#[derive(Default, Clone, Debug)]
struct Visibility {
	from_left: bool,
	from_top: bool,
	from_right: bool,
	from_bottom: bool
}
impl Visibility {
	fn visible(&self) -> bool {
		self.from_top || self.from_left || self.from_right || self.from_bottom
	}
}

#[derive(Default, Clone, Debug)]
struct Viewing {
	from_left: u32,
	from_top: u32,
	from_right: u32,
	from_bottom: u32
}
impl Viewing {
	fn has(&self) -> bool {
		self.from_left > 0 || self.from_top > 0 || self.from_right > 0 || self.from_bottom > 0
	}
	fn score(&self) -> u32 {
		self.from_left * self.from_top * self.from_right * self.from_bottom
	}
}
