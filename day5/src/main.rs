use std::io::{Read, Write};
use std::fs::File;
use std::collections::HashSet;

mod utils;
use utils::*;

fn main() {
	let input: String = {
		let input_path = std::env::args().nth(1).unwrap_or("input.txt".to_string());
		let mut input_file = File::open(input_path).expect("Failed to open the input file");
		let mut input = String::new();
		input_file.read_to_string(&mut input).expect("Failed to read the input");
		input.to_string()
	};

	let lines = input.lines().collect::<Vec<_>>();

	let mut stacks = lines.iter().take_while(|line| !line.is_empty()).vec();
	stacks.remove(stacks.len() - 1);
	stacks.reverse();
	let mut actual_stacks = Vec::new();
	for line in &stacks {
		let mut i = 1;
		let mut s = 0;
		while i < stacks[0].len() {
			if actual_stacks.len() <= s {
				actual_stacks.push(Vec::new());
			}
			if (line.len() > i && line.as_bytes()[i] != b' ') { actual_stacks[s].push(line.as_bytes()[i] as char); }
			i += 4;
			s += 1;
		}
	}

	let commands = lines.iter().skip(stacks.len() + 2)
		.map(|line| {
			let bits = line.split(" ").vec();
			Command {
				quantity: bits[1].parse().unwrap(),
				move_from: bits[3].parse().unwrap(),
				move_to: bits[5].parse().unwrap()
			}
		})
		.vec();
	
	println!("{:?}", actual_stacks);

	let part_2_stacks = actual_stacks.clone();

	for command in &commands {
		let from = &mut actual_stacks[command.move_from - 1];
		let start_rem = from.len() - command.quantity;
		let mut stuff = from.splice(start_rem.., None).vec();
		stuff.reverse();
		let to = &mut actual_stacks[command.move_to - 1];
		to.extend(stuff);
		println!("{:?}", actual_stacks);
	}

	for stack in &actual_stacks {
		print!("{}", stack.iter().last().unwrap());
	}
	println!("");

	let mut actual_stacks = part_2_stacks;

	for command in &commands {
		let from = &mut actual_stacks[command.move_from - 1];
		let start_rem = from.len() - command.quantity;
		let mut stuff = from.splice(start_rem.., None).vec();
		//stuff.reverse();
		let to = &mut actual_stacks[command.move_to - 1];
		to.extend(stuff);
		println!("{:?}", actual_stacks);
	}

	for stack in &actual_stacks {
		print!("{}", stack.iter().last().unwrap());
	}
	println!("");

}

struct Command {
	move_from: usize,
	move_to: usize,
	quantity: usize,
}
