use std::io::{Read, Write};
use std::fs::File;
use std::collections::HashSet;

fn main() {
	let input: String = {
		let input_path = std::env::args().nth(1).unwrap_or("input.txt".to_string());
		let mut input_file = File::open(input_path).expect("Failed to open the input file");
		let mut input = String::new();
		input_file.read_to_string(&mut input).expect("Failed to read the input");
		input.trim().to_string()
	};

	let lines = input.lines().collect::<Vec<_>>();

	let pairs = lines.iter().map(|line| {
		let mut yes = line.split(",");
		let first = yes.next().unwrap();
		let second = yes.next().unwrap();
		(parse_pair(first), parse_pair(second))
	}).collect::<Vec<_>>();

	let count = pairs.iter().filter(|(a, b)| (
		(a.0 <= b.0 && a.1 >= b.1) ||
		(b.0 <= a.0 && b.1 >= a.1)
	)).count();
	println!("{}", count);

	let count = pairs.iter().filter(|(a, b)| (
		(a.0 <= b.1 && a.0 >= b.0) ||
		(a.1 <= b.1 && a.1 >= b.0) ||
		(b.0 <= a.1 && b.0 >= a.0) ||
		(b.1 <= a.1 && b.1 >= a.0)
	)).count();
	println!("{}", count);
}

fn parse_pair(s: &str) -> (usize, usize) {
	let mut yes = s.split("-");
	(yes.next().unwrap().parse().unwrap(), yes.next().unwrap().parse().unwrap())
}
