use std::path::PathBuf;
use std::ops::{Deref, DerefMut};
use std::mem::replace;
use std::io::{Read, Write};
use std::fs::File;
use std::collections::{BTreeMap, VecDeque, HashSet, HashMap};
use regex::{Regex, Match};
use anyhow::{Result, Context, anyhow};
use std::str::Chars;
use std::iter::Peekable;
use std::cmp::Ordering;
use rayon::prelude::*;
use std::collections::BTreeSet;

mod utils;
use utils::*;

const ARENA_WIDTH: usize = 7;

fn main() {
	let input: String = {
		let input_path = std::env::args().nth(1).unwrap_or("input.txt".to_string());
		let mut input_file = File::open(input_path).expect("Failed to open the input file");
		let mut input = String::new();
		input_file.read_to_string(&mut input).expect("Failed to read the input");
		input = input.trim().to_string();
		input
	};

	let lines = input.lines().collect::<Vec<_>>();

	let monkeys: BTreeMap<&str, Monkey<'_>> = lines.iter().map(|line| {
		let bits = line.split(" ").vec();
		let name = bits[0];
		let name = &name[0..name.len()-1];

		let monkey = if bits.len() == 2 {
			Monkey::Number(bits[1].parse().unwrap())
		} else {
			let operation = bits[2];
			match operation {
				"+" => Monkey::Add(bits[1], bits[3]),
				"-" => Monkey::Sub(bits[1], bits[3]),
				"*" => Monkey::Mul(bits[1], bits[3]),
				"/" => Monkey::Div(bits[1], bits[3]),
				_ => panic!("Unknown operation {operation}"),
			}
		};
		(name, monkey)
	}).to_map();

	{
		let mut cached_monkeys = monkeys.iter().map(|(id, monkey)| (*id, CachedMonkey { monkey, result: None })).to_map();
		let mut evaluation_order = vec![vec!["root"]];
		loop {
			let mut next_layer = Vec::new();
			for name in evaluation_order.last().unwrap() {
				let monkey = &monkeys[name];
				next_layer.extend_from_slice(&monkey.depends());
			}
			if next_layer.is_empty() { break; }
			else { evaluation_order.push(next_layer); }
		}

		for layer in evaluation_order.iter().rev() {
			for name in layer {
				let monkey = cached_monkeys.get_mut(name).unwrap();
				if monkey.result.is_none() {
					let result = Some(monkey.monkey.evaluate(&cached_monkeys));
					let monkey = cached_monkeys.get_mut(name).unwrap();
					monkey.result = result;
				}
			}
		}

		println!("Root: {}", cached_monkeys["root"].result.unwrap());

	}

	let branch_a_root = monkeys["root"].depends()[0];
	let branch_b_root = monkeys["root"].depends()[1];
	let mut branch_a_evaluation = vec![vec![branch_a_root]];
	let mut branch_b_evaluation = vec![vec![branch_b_root]];
	loop {
		let mut next_layer = Vec::new();
		for name in branch_a_evaluation.last().unwrap() {
			let monkey = &monkeys[name];
			next_layer.extend_from_slice(&monkey.depends());
		}
		if next_layer.is_empty() { break; }
		else { branch_a_evaluation.push(next_layer); }
	}
	loop {
		let mut next_layer = Vec::new();
		for name in branch_b_evaluation.last().unwrap() {
			let monkey = &monkeys[name];
			next_layer.extend_from_slice(&monkey.depends());
		}
		if next_layer.is_empty() { break; }
		else { branch_b_evaluation.push(next_layer); }
	}

	let human = "humn";
	let root = "root";

	let branch_a_human_count = branch_a_evaluation.iter().map(|v| v.into_iter()).flatten().filter(|h| *h == &human).count();
	let branch_a_has_human = branch_a_human_count > 0;
	let branch_b_human_count = branch_b_evaluation.iter().map(|v| v.into_iter()).flatten().filter(|h| *h == &human).count();
	let branch_b_has_human = branch_b_human_count > 0;
	println!("Human: {} vs {}", branch_a_human_count, branch_b_human_count);
	assert!(branch_a_has_human ^ branch_b_has_human);

	let (forward_evaluation, mut backward_evaluation) = if branch_a_has_human { (branch_b_evaluation, branch_a_evaluation) } else { (branch_a_evaluation, branch_b_evaluation) };
	let (forward_root, backward_root) = if branch_a_has_human { (branch_b_root, branch_a_root) } else { (branch_a_root, branch_b_root) }; 
	let mut cached_monkeys = monkeys.iter().map(|(id, monkey)| (*id, CachedMonkey { monkey, result: None })).to_map();
	for layer in forward_evaluation.iter().rev() {
		for name in layer {
			let monkey = cached_monkeys.get_mut(name).unwrap();
			if monkey.result.is_none() {
				println!("{:?}", monkey.monkey);
				let result = Some(monkey.monkey.evaluate(&cached_monkeys));
				let monkey = cached_monkeys.get_mut(name).unwrap();
				monkey.result = result;
			}
		}
	}
	let top_level_value = cached_monkeys[forward_root].result.unwrap();
	println!("TLV: {} with {}", forward_root, top_level_value);
	for layer in backward_evaluation.iter().rev() {
		for name in layer {
			if name == &human { println!("Did the humna"); continue; }
			let monkey = cached_monkeys.get_mut(name).unwrap();
			if monkey.result.is_none() {
				let result = monkey.monkey.try_evaluate(&cached_monkeys);
				let monkey = cached_monkeys.get_mut(name).unwrap();
				monkey.result = result;
			}
		}
	}
	cached_monkeys.get_mut(&backward_root).unwrap().result = Some(top_level_value);
	backward_evaluation.insert(0, vec![backward_root]);
	for layer in &backward_evaluation {
		for name in layer {
			//Backwards evaluation!
			println!("{}", name);
			let monkey = &cached_monkeys[name];
			let value = monkey.result.unwrap();
			let c = value;
			if matches!(monkey.monkey, Monkey::Number(_)) { continue; }
			let deps = monkey.monkey.depends();
			let children = (cached_monkeys[deps[0]].result, cached_monkeys[deps[1]].result);
			use Monkey::*;
			//a operation b = c
			let mut did_something = true;
			match (monkey.monkey, children) {
				(Add(..), (Some(a), None)) => { cached_monkeys.get_mut(deps[1]).unwrap().result = Some(c - a); },
				(Add(..), (None, Some(b))) => { cached_monkeys.get_mut(deps[0]).unwrap().result = Some(c - b); },
				(Sub(..), (Some(a), None)) => { cached_monkeys.get_mut(deps[1]).unwrap().result = Some(a - c); },
				(Sub(..), (None, Some(b))) => { cached_monkeys.get_mut(deps[0]).unwrap().result = Some(b + c); },
				(Mul(..), (Some(a), None)) => { cached_monkeys.get_mut(deps[1]).unwrap().result = Some(c / a); },
				(Mul(..), (None, Some(b))) => { cached_monkeys.get_mut(deps[0]).unwrap().result = Some(c / b); },
				(Div(..), (Some(a), None)) => { cached_monkeys.get_mut(deps[1]).unwrap().result = Some(a / c); },
				(Div(..), (None, Some(b))) => { cached_monkeys.get_mut(deps[0]).unwrap().result = Some(b * c); },
				(_, (None, None)) => { panic!("How") },
				(_, (Some(_), Some(_))) => { did_something = false; /* Chillin */ },
				(Monkey::Number(_), _) => { panic!("Double how") },
			};
			if did_something {
				println!("{:?} {}", children, c);
				let children = (cached_monkeys[deps[0]].result, cached_monkeys[deps[1]].result);
				println!("{:?} {}", children, c);
			}
		}
	}
	println!("Human: {}", cached_monkeys[human].result.unwrap());

/*	let mut human_val: f64 = 301.0;
	let mut cached_monkeys = monkeys.iter().map(|(id, monkey)| (*id, CachedComplexMonkey { monkey, result: None })).to_map();
	cached_monkeys.get_mut(human).unwrap().result = Some(Complex(vec![0.0,1.0]));

	for order in [&branch_a_evaluation, &branch_b_evaluation] {
		for layer in order.iter().rev() {
			for name in layer {
				let monkey = cached_monkeys.get_mut(name).unwrap();
				if monkey.result.is_none() {
					let result = Some(monkey.monkey.evaluate_complex(&cached_monkeys).unwrap());
					let monkey = cached_monkeys.get_mut(name).unwrap();
					monkey.result = result;
				}
			}
		}
	}
	let branch_a = cached_monkeys[branch_a_root].result.clone().unwrap();
	let branch_b = cached_monkeys[branch_b_root].result.clone().unwrap();
	println!("Branch a: {}", branch_a); 
	println!("Branch b: {}", branch_b);
	let branch_a_result: f64 = branch_a.0.iter().enumerate().map(|(i, n)| *n * human_val.powf(i as f64)).sum();
	let branch_b_result: f64 = branch_b.0.iter().enumerate().map(|(i, n)| *n * human_val.powf(i as f64)).sum();
	println!("Branch a result: {branch_a_result}");
	println!("Branch b result: {branch_b_result}");*/

	/*loop {
		let now = eval(human_val);
		let forward_val = human_val + 1;
		let forward = eval(forward_val);
		let backward_val = human_val - 1;
		let backward = eval(backward_val);
		println!("{:?} <- {:?} -> {:?}", backward, now, forward);

		if (now.0 == now.1) {

		}
		return;
	}*/
	/*for i in 0..i64::MAX {
		let now = eval(i);
		if now.0 == now.1 {
			println!("Human: {}. Root: {}", i, now.0);
			return;
		} else if i % 1000 == 0 {
			println!("Passed {i}");
		}
	}*/

}

#[derive(Debug, Clone)]
enum Monkey<'a> {
	Number(i64),
	Add(&'a str, &'a str),
	Sub(&'a str, &'a str),
	Mul(&'a str, &'a str),
	Div(&'a str, &'a str)
}
impl<'a> Monkey<'a> {
	fn depends(&self) -> Vec<&'a str> {
		match self {
			Monkey::Number(_) => Vec::with_capacity(0),
			Monkey::Add(a, b) => vec![a, b],
			Monkey::Sub(a, b) => vec![a, b],
			Monkey::Mul(a, b) => vec![a, b],
			Monkey::Div(a, b) => vec![a, b],
		}
	}
	fn try_evaluate(&self, cached_monkeys: &BTreeMap<&str, CachedMonkey<'_, '_>>) -> Option<i64> {
		Some(match self{
			Monkey::Number(num) => *num,
			Monkey::Add(a, b) => cached_monkeys[a].result? + cached_monkeys[b].result?,
			Monkey::Sub(a, b) => cached_monkeys[a].result? - cached_monkeys[b].result?,
			Monkey::Mul(a, b) => cached_monkeys[a].result? * cached_monkeys[b].result?,
			Monkey::Div(a, b) => cached_monkeys[a].result? / cached_monkeys[b].result?,
		})
	}
	fn evaluate(&self, cached_monkeys: &BTreeMap<&str, CachedMonkey<'_, '_>>) -> i64 { self.try_evaluate(cached_monkeys).unwrap() }

	fn evaluate_complex(&self, cached_monkeys: &BTreeMap<&str, CachedComplexMonkey>) -> Option<Complex> {
		Some(match self{
			Monkey::Number(num) => Complex(vec![*num as f64]),
			Monkey::Add(a, b) => cached_monkeys[a].result.clone()? + cached_monkeys[b].result.clone()?,
			Monkey::Sub(a, b) => cached_monkeys[a].result.clone()? - cached_monkeys[b].result.clone()?,
			Monkey::Mul(a, b) => cached_monkeys[a].result.clone()? * cached_monkeys[b].result.clone()?,
			Monkey::Div(a, b) => cached_monkeys[a].result.clone()? / cached_monkeys[b].result.clone()?,
		})
	}
}

#[derive(Debug, Clone)]
struct CachedMonkey<'a, 'm> {
	monkey: &'m Monkey<'a>,
	result: Option<i64>,
}

#[derive(Debug, Clone)]
struct CachedComplexMonkey<'a, 'm> {
	monkey: &'m Monkey<'a>,
	result: Option<Complex>,
}

#[derive(Debug, Clone)]
struct Complex(Vec<f64>);
impl std::ops::Add<Complex> for Complex {
	type Output = Complex;
	fn add(self, other: Complex) -> Complex {
		let size = self.0.len().max(other.0.len());
		Complex((0..size).map(|i| self.0.get(i).cloned().unwrap_or(0.0) + other.0.get(i).cloned().unwrap_or(0.0)).vec())
	}
}
impl std::ops::Sub<Complex> for Complex {
	type Output = Complex;
	fn sub(self, other: Complex) -> Complex {
		let size = self.0.len().max(other.0.len());
		Complex((0..size).map(|i| self.0.get(i).cloned().unwrap_or(0.0) - other.0.get(i).cloned().unwrap_or(0.0)).vec())
	}
}
impl std::ops::Mul<Complex> for Complex {
	type Output = Complex;
	fn mul(self, other: Complex) -> Complex {
		let size = self.0.len().max(other.0.len());
		let mut out = (0..(self.0.len() * other.0.len() + 1)).map(|_| 0.0).vec();
		for ia in 0..size {
			for ib in 0..size {
				let a = self.0.get(ia).cloned().unwrap_or(0.0);
				let b = other.0.get(ib).cloned().unwrap_or(0.0);
				let ir = ia + ib;
				let r = a * b;
				out[ir] += r;
			}
		}
		Complex(out)
	}
}
impl std::ops::Div<Complex> for Complex {
	type Output = Complex;
	fn div(self, other: Complex) -> Complex {
		let dividend = self.0;
		let divisor = other.0;
		assert!(divisor.len() <= dividend.len());

		let operations = dividend.len() - divisor.len() + 1;
		let mut quotient: Vec<f64> = Vec::new();
		let mut dividend_bits = dividend.iter().rev().cloned();
		let mut local_dividend = VecDeque::new();
		while local_dividend.len() < divisor.len() {
			local_dividend.push_front(dividend_bits.next().unwrap());
		}
		for i in 0..operations {
			assert!(local_dividend.len() == divisor.len());
			let local_quotient = local_dividend.iter().last().unwrap() / divisor.last().unwrap();
			for (i, val) in local_dividend.iter_mut().enumerate() { *val -= local_quotient * divisor[i]; }
			if *local_dividend.iter().last().unwrap() != 0.0 {
				panic!("Invalid remainder? {:?} {:?}", dividend, divisor);
			}
			quotient.insert(0, local_quotient);
			local_dividend.pop_back();
			if let Some(next) = dividend_bits.next() { local_dividend.push_front(next); }
		}
		println!("Remainder when div: {}", Complex(local_dividend.into_iter().vec()));

		Complex(quotient)
	}
}
impl std::fmt::Display for Complex {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		let mut iter = self.0.iter().enumerate().rev().peekable();
		while let Some((i, n)) = iter.next() {
			if *n != 0.0 || i == 0 {
				if *n != 1.0 { n.fmt(f)?; }
				if i == 1 { f.write_str("x")?; }
				else if i != 0 {
					f.write_str("x^")?;
					i.fmt(f)?;
				}
				if iter.peek().is_some() { f.write_str(" + ")?; }
			}
		}
		Ok(())
	}
}

