use std::path::PathBuf;
use std::ops::{Deref, DerefMut};
use std::mem::replace;
use std::io::{Read, Write};
use std::fs::File;
use std::collections::{BTreeMap, VecDeque, HashSet, HashMap};
use regex::{Regex, Match};
use anyhow::{Result, Context, anyhow};
use std::str::Chars;
use std::iter::Peekable;
use std::cmp::Ordering;
use rayon::prelude::*;
use std::collections::BTreeSet;

mod utils;
use utils::*;

const ARENA_WIDTH: usize = 7;

fn main() {
	let input: String = {
		let input_path = std::env::args().nth(1).unwrap_or("input.txt".to_string());
		let mut input_file = File::open(input_path).expect("Failed to open the input file");
		let mut input = String::new();
		input_file.read_to_string(&mut input).expect("Failed to read the input");
		input = input.trim().to_string();
		input
	};

	let lines = input.lines().collect::<Vec<_>>();
	
	let nums = lines.iter().enumerate().map(|(i, line)| Num { n: line.parse().unwrap(), original_pos: i }).vec();
	println!("Nums: {:?}", nums.iter().map(|n| **n).vec());

	let mut mixed = nums.clone();
	for mixing_i in 0..nums.len() {
		let (current_pos, _) = mixed.iter().enumerate().find(|(_, num)| num.original_pos == mixing_i).unwrap();
		let num = mixed.remove(current_pos);
		let mut new_pos = current_pos as i64 + *num;
		//if *num < 0 { new_pos -= 1; }
		while new_pos < 0 { new_pos += mixed.len() as i64; }
		while new_pos >= mixed.len() as i64 { new_pos -= mixed.len() as i64; }
		if new_pos == 0 { new_pos = mixed.len() as i64; }
		mixed.insert(new_pos as usize, num);
		//println!("Mixed: {:?}", mixed.iter().map(|n| **n).vec());
	}

	println!("Mixed: {:?}", mixed.iter().map(|n| **n).vec());
	let (zero_index, _) = mixed.iter().enumerate().find(|(_, num)| ***num == 0).unwrap();
	let coords = [mixed[(zero_index + 1000) % mixed.len()], mixed[(zero_index + 2000) % mixed.len()], mixed[(zero_index + 3000) % mixed.len()]];
	println!("Coordinates: {} {} {}", coords[0], coords[1], coords[2]);
	let sum: i64 = coords.iter().map(|v| **v).sum();
	println!("Coordinate sum: {}", sum);

	let nums = {
		let mut nums = nums;
		for num in &mut nums {
			**num *= 811589153;
		}
		nums
	};
	let mut mixed = nums.clone();
	for mixing_round in 0..10 {
		for mixing_i in 0..nums.len() {
			let (current_pos, _) = mixed.iter().enumerate().find(|(_, num)| num.original_pos == mixing_i).unwrap();
			let num = mixed.remove(current_pos);
			let mut new_pos = current_pos as i64 + *num;
			//if *num < 0 { new_pos -= 1; }
			//while new_pos >= mixed.len() as i64 { new_pos -= mixed.len() as i64; }
			new_pos = new_pos % (mixed.len() as i64);
			while new_pos < 0 { new_pos += mixed.len() as i64; }
			if new_pos == 0 { new_pos = mixed.len() as i64; }
			mixed.insert(new_pos as usize, num);
			//println!("Mixed: {:?}", mixed.iter().map(|n| **n).vec());
		}
		println!("Finished round {mixing_round}");
	}

	println!("Mixed: {:?}", mixed.iter().map(|n| **n).vec());
	let (zero_index, _) = mixed.iter().enumerate().find(|(_, num)| ***num == 0).unwrap();
	let coords = [mixed[(zero_index + 1000) % mixed.len()], mixed[(zero_index + 2000) % mixed.len()], mixed[(zero_index + 3000) % mixed.len()]];
	println!("Coordinates: {} {} {}", coords[0], coords[1], coords[2]);
	let sum: i64 = coords.iter().map(|v| **v).sum();
	println!("Coordinate sum: {}", sum);
}

#[derive(Debug, Copy, Clone)]
struct Num {
	n: i64,
	original_pos: usize,
}
impl Deref for Num {
	type Target = i64;
	fn deref(&self) -> &i64 { &self.n }
}
impl DerefMut for Num {
	fn deref_mut(&mut self) -> &mut i64 { &mut self.n }
}
impl std::fmt::Display for Num {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result { self.n.fmt(f) }
}
