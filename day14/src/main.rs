use std::path::PathBuf;
use std::mem::replace;
use std::io::{Read, Write};
use std::fs::File;
use std::collections::{VecDeque, HashSet, HashMap};
use regex::{Regex, Match};
use anyhow::{Result, Context, anyhow};
use std::str::Chars;
use std::iter::Peekable;
use std::cmp::Ordering;

mod utils;
use utils::*;

fn main() {
	let input: String = {
		let input_path = std::env::args().nth(1).unwrap_or("input.txt".to_string());
		let mut input_file = File::open(input_path).expect("Failed to open the input file");
		let mut input = String::new();
		input_file.read_to_string(&mut input).expect("Failed to read the input");
		input = input.trim().to_string();
		input
	};

	let lines = input.lines().collect::<Vec<_>>();

	let mut commands: Vec<Vec<GridLoc>> = lines.iter().map(|line| {
		line.split("->").map(|point| {
			let mut bits = point.trim().split(",");
			(bits.next().unwrap().parse().unwrap(), bits.next().unwrap().parse().unwrap())
		}).vec()
	}).vec();

	let dimensions = commands.iter().map(|cmd| cmd.iter()).flatten().cloned().reduce(|(ax, ay), (bx, by)| (ax.max(bx), ay.max(by))).unwrap();
	let dimensions = (dimensions.0 + 1, dimensions.1 + 1);
	let grid_data = vec![Cell::Air; dimensions.0 * dimensions.1];
	let mut grid = GridBuffer::from_raw(dimensions.0, dimensions.1, grid_data);
	let mut grid = grid.grid_mut();

	for command in &commands {
		let mut iter = command.iter().cloned();
		let mut prev = iter.next().unwrap();
		while let Some(next) = iter.next() {
			let (ax, ay) = prev;
			let (bx, by) = next;
			let sx = ax.min(bx);
			let sy = ay.min(by);
			let ex = ax.max(bx);
			let ey = ay.max(by);
			println!("{} {} {} {}", sx, sy, ex, ey);
			for x in sx..=ex {
				for y in sy..=ey {
					grid[(x, y)] = Rock
				}
			}

			prev = next;
		}
	}

	for y in 0..grid.height() {
		for x in 400..grid.width() {
			print!("{}", grid[(x, y)].str());
		}
		println!("");
	}

	let mut count = 0;
	while let Some(_drop_loc) = drop_sand(&mut grid) { count += 1; }
	println!("Sands dropped: {}", count);


	let dimensions = commands.iter().map(|cmd| cmd.iter()).flatten().cloned().reduce(|(ax, ay), (bx, by)| (ax.max(bx), ay.max(by))).unwrap();
	let dimensions = ((dimensions.0 + 1) * 2, dimensions.1 + 1   /**/      /**/  + 2  /**/              );
	let grid_data = vec![Cell::Air; dimensions.0 * dimensions.1];
	let mut grid = GridBuffer::from_raw(dimensions.0, dimensions.1, grid_data);
	let mut grid = grid.grid_mut();
	commands.push(vec![(0, dimensions.1 - 1), (dimensions.0 - 1, dimensions.1 - 1)]);
	for command in &commands {
		let mut iter = command.iter().cloned();
		let mut prev = iter.next().unwrap();
		while let Some(next) = iter.next() {
			let (ax, ay) = prev;
			let (bx, by) = next;
			let sx = ax.min(bx);
			let sy = ay.min(by);
			let ex = ax.max(bx);
			let ey = ay.max(by);
			println!("{} {} {} {}", sx, sy, ex, ey);
			for x in sx..=ex {
				for y in sy..=ey {
					grid[(x, y)] = Rock
				}
			}

			prev = next;
		}
	}

	let mut count = 0;
	while let Some(drop_loc) = drop_sand_v2(&mut grid) {
		count += 1;
		if drop_loc == SAND_SPAWN {
			println!("YES");
			break;
		}
	}
	println!("Sands dropped: {}", count);

	/*for y in 0..grid.height() {
		for x in 400..grid.width() {
			print!("{}", grid[(x, y)].str());
		}
		println!("");
	}*/
}

const SAND_SPAWN: GridLoc = (500, 0);

fn drop_sand(grid: &mut Grid<Cell, &mut GridBuffer<Cell>>) -> Option<GridLoc> {
	let mut loc = SAND_SPAWN;
	loop {
		let (mut x, mut y) = loc;
		y += 1;
		//Check below
		if matches!(grid.get((x, y)).ok()?, Air) {
			loc = (x, y);
			continue;
		}
		//Check below-left
		if let Some(x) = loc.0.checked_sub(1) {
			if matches!(grid.get((x, y)).ok()?, Air) {
				loc = (x, y);
				continue;
			}
		}
		//Check below-right
		{
			x += 1;
			if matches!(grid.get((x, y)).ok()?, Air) {
				loc = (x, y);
				continue;
			}
		}
		//It's stationary
		grid[loc] = Sand;
		return Some(loc);
	}
}
fn drop_sand_v2(grid: &mut Grid<Cell, &mut GridBuffer<Cell>>) -> Option<GridLoc> {
	let mut loc = SAND_SPAWN;
	loop {
		let (mut x, mut y) = loc;
		y += 1;
		//Check below
		if matches!(grid.get((x, y)).ok()?, Air) {
			loc = (x, y);
			continue;
		}
		//Check below-left
		if let Some(x) = loc.0.checked_sub(1) {
			if matches!(grid.get((x, y)), Ok(Air)) {
				loc = (x, y);
				continue;
			}
		}
		//Check below-right
		{
			x += 1;
			if matches!(grid.get((x, y)), Ok(Air)) {
				loc = (x, y);
				continue;
			}
		}
		//It's stationary
		grid[loc] = Sand;
		return Some(loc);
	}
}

#[derive(Debug, Clone)]
enum Cell {
	Air,
	Rock,
	Sand
}
use Cell::*;
impl Cell {
	fn str(&self) -> &'static str {
		match self {
			Air => ".",
			Rock => "#",
			Sand => "O",
		}
	}
}
