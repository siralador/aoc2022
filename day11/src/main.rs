use std::path::PathBuf;
use std::mem::replace;
use std::io::{Read, Write};
use std::fs::File;
use std::collections::{VecDeque, HashSet, HashMap};
use regex::{Regex, Match};
use anyhow::{Result, Context, anyhow};

mod utils;
use utils::*;

fn main() {
	let input: String = {
		let input_path = std::env::args().nth(1).unwrap_or("input.txt".to_string());
		let mut input_file = File::open(input_path).expect("Failed to open the input file");
		let mut input = String::new();
		input_file.read_to_string(&mut input).expect("Failed to read the input");
		input = input.trim().to_string();
		input
	};

	let lines = input.lines().collect::<Vec<_>>();

	let mut monkeys = parse(&lines);

	println!("{:?}", monkeys);

	for _ in 0..20 {
		do_round(&mut monkeys);
	}

	let mut activity = monkeys.iter().map(|monkey| monkey.items_inspected).vec();
	activity.sort();
	activity.reverse();
	let monkey_busieness =activity[0] * activity[1];
	println!("{}", monkey_busieness);

	let mut monkeys = parse(&lines);

	let upper_modulo = 20;
	let magic_modulo = monkeys.iter().map(|monkey| monkey.divisible_by).product();
	//let magic_modulo = (1u128..20).product();
	/*'find_modulo: loop {
		magic_modulo += upper_modulo;
		for m in 1..=upper_modulo {
			if magic_modulo % m != 0 { continue 'find_modulo; }
		}
		break;
	}
	println!("Magic modulo: {}", magic_modulo);*/

	for w in 0..10000 {
		if w % 1000 == 0 {
			let mut activity = monkeys.iter().map(|monkey| monkey.items_inspected).vec();
			activity.sort();
			activity.reverse();
			let monkey_busieness =activity[0] * activity[1];
			println!("{:?}", activity);
			println!("{} = {}", w, monkey_busieness);
		}
		do_anxiety_round(&mut monkeys, magic_modulo);
	}

	let mut activity = monkeys.iter().map(|monkey| monkey.items_inspected).vec();
	activity.sort();
	activity.reverse();
	let monkey_busieness =activity[0] * activity[1];
	println!("{:?}", activity);
	println!("{}", monkey_busieness);
}

fn parse(lines: &Vec<&str>) -> Vec<Monkey> {
	let mut monkey_parse = lines.iter();
	let mut monkeys = Vec::new();
	while let Some(_monkey) = monkey_parse.next() {
		let items = monkey_parse.next().unwrap();
		let items = items[items.find(": ").unwrap()+2..].split(", ").map(|num| num.parse().unwrap()).vec();
		let operation_text = monkey_parse.next().unwrap();
		assert!(operation_text.starts_with("  Operation: new = old "));
		let operation_bits = operation_text["  Operation: new = old ".len()..].split(" ").vec();
		let operation = operation_bits[0];
		let opperand = operation_bits[1];
		let operation: Box<dyn FnMut(u128) -> u128> = if opperand == "old" {
			match operation {
				"+" => Box::new(move |input: u128| input + input),
				"-" => Box::new(move |input: u128| input - input),
				"*" => Box::new(move |input: u128| input * input),
				"/" => panic!("divide"),
				_ => panic!("unknown opeation"),
			}
		} else {
			let opperand: u128 = operation_bits[1].parse().unwrap();
			match operation {
				"+" => Box::new(move |input: u128| input + opperand),
				"-" => Box::new(move |input: u128| input - opperand),
				"*" => Box::new(move |input: u128| input * opperand),
				"/" => panic!("divide"),
				_ => panic!("unknown opeation"),
			}
		};
		let test = monkey_parse.next().unwrap();
		let divisible_by = test.split(" ").last().unwrap().parse().unwrap();
		let throw_to_true = monkey_parse.next().unwrap();
		let throw_to_false = monkey_parse.next().unwrap();
		let throw_to_true = throw_to_true.split(" ").last().unwrap().parse().unwrap();
		let throw_to_false = throw_to_false.split(" ").last().unwrap().parse().unwrap();
		let _blank = monkey_parse.next();//.unwrap();

		let monkey = Monkey {
			items,
			operation,
			divisible_by,
			throw_to_true,
			throw_to_false,
			items_inspected: 0,
		};
		monkeys.push(monkey);
	}
	monkeys
}

fn do_round(monkeys: &mut Vec<Monkey>) {
	for i in 0..monkeys.len() {
		while !monkeys[i].items.is_empty() {
			let item = monkeys[i].items.remove(0);
			monkeys[i].items_inspected += 1;
			let item = (monkeys[i].operation)(item as u128);
			let item = item / 3;
			let target = if item % monkeys[i].divisible_by == 0 { monkeys[i].throw_to_true }
			else { monkeys[i].throw_to_false };
			monkeys[target].items.push(item);
		}
	}
}

fn do_anxiety_round(monkeys: &mut Vec<Monkey>, magic_modulo: u128) {
	for i in 0..monkeys.len() {
		while !monkeys[i].items.is_empty() {
			let item = monkeys[i].items.remove(0);
			monkeys[i].items_inspected += 1;
			let mut item = (monkeys[i].operation)(item);
			let magic = item % magic_modulo;
			/*for i in 1..=magic_modulo {
				if magic % i != item % i {
					panic!("{} % {} = {}; {} % {} = {};", magic, i, magic % i, item, i, item % i);
				}
			}*/
			item = magic;
			//let item = item / 3;
			let test = monkeys[i].divisible_by;
			let target = if item % test == 0 { monkeys[i].throw_to_true }
			else { monkeys[i].throw_to_false };
			/*let mut trunc_item = item & 0b1111111111111111;
			assert!(trunc_item <= item);
			trunc_item -= 1;
			'yes: loop {
				trunc_item += 1;
				for test in 2..50 {
					if trunc_item % test != item % test {
						continue 'yes;
					}
				}
				break;
			}
			let item = trunc_item;*/
			monkeys[target].items.push(item);
		}
	}
}

struct Monkey {
	items: Vec<u128>,
	operation: Box<dyn FnMut(u128) -> u128>,
	divisible_by: u128,
	throw_to_true: usize,
	throw_to_false: usize,
	items_inspected: u128,
}
#[derive(Debug)]
struct MonkeyDisplay<'a> {
	items: &'a Vec<u128>,
	divisible_by: u128,
	throw_to_true: usize,
	throw_to_false: usize,
	items_inspected: u128,
}
impl std::fmt::Debug for Monkey {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		MonkeyDisplay {
			items: &self.items,
			divisible_by: self.divisible_by,
			throw_to_true: self.throw_to_true,
			throw_to_false: self.throw_to_false,
			items_inspected: self.items_inspected,
		}.fmt(f)
	}
}
