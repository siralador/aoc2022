use std::path::PathBuf;
use std::ops::{Deref, DerefMut};
use std::mem::replace;
use std::io::{Read, Write};
use std::fs::File;
use std::collections::{BTreeMap, VecDeque, HashSet, HashMap};
use regex::{Regex, Match};
use anyhow::{Result, Context, anyhow};
use std::str::Chars;
use std::iter::Peekable;
use std::cmp::Ordering;
use rayon::prelude::*;
use std::collections::BTreeSet;

mod utils;
use utils::*;

const ARENA_WIDTH: usize = 7;

fn main() {
	let input: String = {
		let input_path = std::env::args().nth(1).unwrap_or("input.txt".to_string());
		let mut input_file = File::open(input_path).expect("Failed to open the input file");
		let mut input = String::new();
		input_file.read_to_string(&mut input).expect("Failed to read the input");
		input = input.trim().to_string();
		input
	};

	let lines = input.lines().collect::<Vec<_>>();

	let width = lines[0].len() as isize;
	let height = lines.len() as isize;
	let original_blizzards = lines.iter().enumerate().map(|(y, line)| {
		line.chars().enumerate().filter_map(move |(x, c)| {
			let tile = match c {
				'#' => Wall,
				'>' => Blizzard(Right),
				'<' => Blizzard(Left),
				'^' => Blizzard(Up),
				'v' => Blizzard(Down),
				'.' => Ground,
				_ => panic!("Unknown"),
			};
			if let Blizzard(direction) = tile { Some(((x as isize, y as isize), direction)) }
			else { None }
		})
	})
	.flatten()
	.vec();

	let horizontal_period = width - 2;
	let vertical_period = height - 2;
	println!("{horizontal_period} {vertical_period}");
	let mut heads: BTreeSet<Pos> = BTreeSet::new();
	let mut blizzards = original_blizzards.clone();
	let start = (1, 0);
	let end = (width - 2, height - 1);
	heads.insert(start);

	let blizzard_sort = |b: &BlizzardDef| b.0;
	blizzards.sort_by_key(blizzard_sort);

	let advance_blizzards = move |blizzards: &mut Vec<BlizzardDef>| {
		for (pos, direction) in blizzards.iter_mut() {
			*pos = add(*pos, direction.vector());
			if pos.0 == 0 { pos.0 = width - 2; }
			if pos.1 == 0 { pos.1 = height - 2; }
			if pos.0 >= width - 1 { pos.0 = 1; }
			if pos.1 >= height - 1 { pos.1 = 1; }
		}
		blizzards.sort_by_key(blizzard_sort);
	};

	let pos_check = move |pos: Pos| (pos.0 > 0 && pos.1 > 0 && pos.0 < width - 1 && pos.1 < height - 1) || pos == end || pos == start;
	
	let mut current_blizzards = blizzards;
	print_blizzards(&current_blizzards, width, height);

	let do_turn = move |current_blizzards: &mut Vec<BlizzardDef>, heads: &mut BTreeSet<Pos>, goal: Pos| {
		let mut new_blizzards = current_blizzards.clone();
		advance_blizzards(&mut new_blizzards);
		let new_heads: Vec<_> = heads.par_iter()
			.map(|pos| {
				let blizzard_approaching_this_spot = new_blizzards.binary_search_by_key(pos, blizzard_sort).is_ok();
				let can_wait = !blizzard_approaching_this_spot;
				let move_options = [Up, Down, Left, Right];
				let mut branches = move_options.into_iter()
					.filter_map(|direction| {
						let new_pos = add(*pos, direction.vector());
						/*//Cannot pass through a blizzard head on
						if let Ok(head_on_collision_index) = current_blizzards.binary_search_by_key(&new_pos, blizzard_sort) {
							let colliding_blizzard = &current_blizzards[head_on_collision_index];
							let head_on_collision = colliding_blizzard.1.opposite() == direction;
							if head_on_collision { None }
							else { Some(new_pos) }
						}
						else { Some(new_pos) }*/
						Some(new_pos)
					})
					.filter(|new_pos| pos_check(*new_pos))
					.filter(|new_pos| new_blizzards.binary_search_by_key(new_pos, blizzard_sort).is_err())
					.vec();
				if can_wait { branches.push(*pos); }
				branches
			})
			.collect();
		heads.clear();
		let mut goal_reached = false;
		for head in new_heads.into_iter().flatten() {
			if head == goal {
				goal_reached = true;
				break;
			}
			heads.insert(head);
		}
		*current_blizzards = new_blizzards;
		goal_reached
	};
	macro_rules! do_turn_advanced {
		($goal:expr) => {
			|turn: &usize| {
				if do_turn(&mut current_blizzards, &mut heads, $goal) {
					println!("Finished on turn {turn}");
					true
				}
				else {
					println!("Turn {turn}; Heads: {}", heads.len());
					false
				}
			}

		}
	}
	let there = (1..).find(do_turn_advanced!(end)).unwrap();
	heads.clear();
	heads.insert(end);
	let back = (there+1..).find(do_turn_advanced!(start)).unwrap();
	heads.clear();
	heads.insert(start);
	let there_again = (back+1..).find(do_turn_advanced!(end)).unwrap();
	println!("{}", there_again);
}

type Pos = (isize, isize);
fn add(a: Pos, b: Pos) -> Pos { (a.0 + b.0, a.1 + b.1) }
fn sub(a: Pos, b: Pos) -> Pos { (a.0 - b.0, a.1 - b.1) }
type BlizzardDef = (Pos, Direction);


#[derive(Debug, Clone, PartialEq, Eq)]
enum Tile {
	Wall,
	Blizzard(Direction),
	Ground
}
use Tile::*;
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
enum Direction {
	Up,
	Down,
	Right,
	Left
}
use Direction::*;
impl Direction {
	fn vector(&self) -> Pos {
		match self {
			Up => (0, -1),
			Down => (0, 1),
			Left => (-1, 0),
			Right => (1, 0),
		}
	}
	fn opposite(&self) -> Direction { 
		match self {
			Up => Down,
			Down => Up,
			Left => Right,
			Right => Left,
		}
	}
	fn c(&self) -> &'static str {
		match self {
			Up => "^",
			Down => "v",
			Left => "<",
			Right => ">",
		}
	}
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
struct State {
	pos: GridLoc,
}

fn print_blizzards(blizzards: &Vec<BlizzardDef>, width: isize, height: isize) {
	for _ in 0..width { print!("@"); }
	println!("");
	for y in 1..(height-1) {
		print!("@");
		for x in 1..(width-1) {
			let blizzards_here = blizzards.iter().filter(|(pos,_)| *pos == (x, y)).vec();
			if blizzards_here.len() > 9 { print!("B"); }
			else if blizzards_here.len() > 1 { print!("{}", blizzards_here.len()); }
			else if blizzards_here.len() == 1 { print!("{}", blizzards_here[0].1.c()); }
			else { print!("."); }
		}
		print!("@");
		println!("");
	}
	for _ in 0..width { print!("@"); }
	println!("\n\n\n");
}
