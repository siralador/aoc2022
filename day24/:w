use std::path::PathBuf;
use std::ops::{Deref, DerefMut};
use std::mem::replace;
use std::io::{Read, Write};
use std::fs::File;
use std::collections::{BTreeMap, VecDeque, HashSet, HashMap};
use regex::{Regex, Match};
use anyhow::{Result, Context, anyhow};
use std::str::Chars;
use std::iter::Peekable;
use std::cmp::Ordering;
use rayon::prelude::*;
use std::collections::BTreeSet;

mod utils;
use utils::*;

const ARENA_WIDTH: usize = 7;

fn main() {
	let input: String = {
		let input_path = std::env::args().nth(1).unwrap_or("input.txt".to_string());
		let mut input_file = File::open(input_path).expect("Failed to open the input file");
		let mut input = String::new();
		input_file.read_to_string(&mut input).expect("Failed to read the input");
		//input = input.trim().to_string();
		input
	};

	let lines = input.lines().collect::<Vec<_>>();

	let board_lines = lines.iter().take_while(|line| !line.is_empty()).vec();
	let instruction_line = lines.iter().skip(board_lines.len() + 1).next().unwrap();

	let mut board: BTreeMap<GridLoc, Tile> = BTreeMap::new();
	let max_y = board_lines.len();
	let mut max_x = 1;
	for (i, line) in board_lines.iter().enumerate() {
		let y = i + 1;
		for (i, c) in line.chars().enumerate() {
			let x = i + 1;
			max_x = max_x.max(x);
			let tile = match c {
				'.' => Some(Ground),
				'#' => Some(Wall),
				_ => None
			};
			if let Some(tile) = tile { board.insert((x, y), tile); }
		}
	}
	let max_x = max_x;
	let board = board;

	let mut instruction_iter = instruction_line.chars().peekable();
	let mut instructions = vec![];
	while let Some(c) = instruction_iter.next() {
		if c.is_digit(10) {
			let mut num = String::new();
			num.push(c);
			while let Some(c) = instruction_iter.peek() {
				if c.is_digit(10) { num.push(instruction_iter.next().unwrap()); }
				else { break; }
			}
			instructions.push(Forward(num.parse().unwrap()));
		}
		else if c == 'L' { instructions.push(TurnLeft); }
		else if c == 'R' { instructions.push(TurnRight); }
	}

	let enforce_pos = move |mut pos: GridLoc| {
		if pos.0 == 0 { pos.0 = max_x; }
		if pos.1 == 0 { pos.1 = max_y; }
		if pos.0 > max_x { pos.0 = 1; }
		if pos.1 > max_y { pos.1 = 1; }
		pos
	};

	let begin = board.keys().cloned().reduce(|(ax, ay), (bx, by)| if ay < by || (ay == by && ax < bx) { (ax, ay) } else { (bx, by) }).unwrap();
	let mut pos = begin;
	let mut facing = FRight;
	for (i, instruction) in instructions.iter().enumerate() {
		println!("Instruction {}", i);
		match instruction {
			Forward(amount) => {
				let mut new_pos = pos;
				'moving: for _ in 0..*amount {
					new_pos = enforce_pos(facing.apply(new_pos));
					while board.get(&new_pos).is_none() {
						new_pos = enforce_pos(facing.apply(new_pos));
					}
					if board.get(&new_pos) == Some(&Wall) { break 'moving; }
					assert!(board.get(&new_pos) == Some(&Ground));
					pos = new_pos;
				}
			},
			TurnLeft => { facing = facing.turn_left(); },
			TurnRight => { facing = facing.turn_right(); },
		}
		//print_state(&board, pos, facing, max_x, max_y);
	}
	println!("{:?} {:?}", pos, facing);
	println!("{}", (1000 * pos.1) + (4 * pos.0) + facing.num());

	let section_size = 50;
	let segsiz = section_size;
	let mut sections:  BTreeMap<CubeFace, GridBuffer<Tile>> = BTreeMap::new();
	fn deduce_sections(board: &BTreeMap<GridLoc, Tile>, sections: &mut BTreeMap<CubeFace, GridBuffer<Tile>>, section: GridLoc, segsiz: usize, my_face: CubeFace, rotation: Rotation) {
		let origin = (section.0 * segsiz + 1, section.1 * segsiz + 1);
		assert!(board.get(&origin).is_some());
		sections.insert(
			my_face,
			GridBuffer::from_raw(
				segsiz,
				segsiz,
				(0..segsiz)
				.map(
					|y| (0..segsiz)
						.map(move |x| (x, y))
						.map(|loc| rotation.rotate(loc, segsiz))
				)
				.flatten()
				.map(|(x, y)| (x + origin.0, y + origin.1))
				.map(|loc| board[&loc])
				.vec()
			)
		);

		if !sections.contains_key(&my_face.to_up()) {
			if let Some(y) = section.1.checked_sub(1) {
				let section = (section.0, y);
				let new_origin = (section.0 * segsiz + 1, section.1 * segsiz + 1);
				if board.get(&new_origin).is_some() {
					deduce_sections(board, sections, section, segsiz, my_face.to_up(), rotation);
				}
			}
		}
		if !sections.contains_key(&my_face.to_down()) {
			let section = (section.0, section.1 + 1);
			let new_origin = (section.0 * segsiz + 1, section.1 * segsiz + 1);
			if board.get(&new_origin).is_some() {
				deduce_sections(board, sections, section, segsiz, my_face.to_down(), rotation);
			}
		}
		if !sections.contains_key(&my_face.to_left()) {
			if let Some(x) = section.0.checked_sub(1) {
				let section = (x, section.1);
				let new_origin = (section.0 * segsiz + 1, section.1 * segsiz + 1);
				if board.get(&new_origin).is_some() {
					deduce_sections(board, sections, section, segsiz, my_face.to_left(), rotation);
				}
			}
		}
		if !sections.contains_key(&my_face.to_up()) {
			if let Some(y) = section.1.checked_sub(1) {
				let section = (section.0, y);
				let new_origin = (section.0 * segsiz + 1, section.1 * segsiz + 1);
				if board.get(&new_origin).is_some() {
					deduce_sections(board, sections, section, segsiz, my_face.to_up(), rotation);
				}
			}
		}
	}
	deduce_sections(&board, &mut sections, (begin.0 / section_size, begin.1 / section_size), section_size, Top, R0);
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
enum Tile {
	Ground,
	Wall
}
use Tile::*;
impl Tile {
	fn str(&self) -> &'static str {
		match self{
			Ground => ".",
			Wall => "#",
		}
	}
}

#[derive(Debug, Clone)]
enum Instruction {
	Forward(usize),
	TurnLeft,
	TurnRight,
}
use Instruction::*;

#[derive(Debug, Clone, Copy)]
enum Facing {
	FUp,
	FRight,
	FDown,
	FLeft
}
use Facing::*;
impl Facing {
	fn turn_left(&self) -> Facing {
		match self {
			FUp => FLeft,
			FRight => FUp,
			FDown => FRight,
			FLeft => FDown,
		}
	}
	fn turn_right(&self) -> Facing {
		match self {
			FUp => FRight,
			FRight => FDown,
			FDown => FLeft,
			FLeft => FUp,
		}
	}
	fn apply(&self, (x, y): GridLoc) -> GridLoc {
		match self {
			FUp => (x, y-1),
			FRight => (x+1, y),
			FDown => (x, y+1),
			FLeft => (x-1, y),
		}
	}
	fn num(&self) -> usize {
		match self {
			FRight => 0,
			FDown => 1,
			FLeft => 2,
			FUp => 3,
		}
	}
	fn str(&self) -> &'static str {
		match self {
			FRight => ">",
			FDown => "_",
			FLeft => "<",
			FUp => "T",
		}
	}
}

fn print_state(board: &BTreeMap<GridLoc, Tile>, pos: GridLoc, facing: Facing, max_x: usize, max_y: usize) {
	for y in 1..=max_y {
		for x in 1..=max_x {
			if (x, y) == pos {
				print!("{}", facing.str());
			} else {
				if let Some(tile) = board.get(&(x, y)) { print!("{}", tile.str()); }
				else { print!(" "); }
			}
		}
		println!("");
	}
	println!("\n\n");
}

//clockwise
#[derive(Debug, Clone, Copy)]
enum Rotation {
	R0,
	R90,
	R180,
	R270,
}
use Rotation::*;
impl Rotation {
	fn rotate(&self, loc: GridLoc, segsiz: usize) -> GridLoc {
		match self {
			R0 => loc,
			R90 => (segsiz - loc.1 - 1, loc.0),
			R180 => (segsiz - loc.0 - 1, segsiz - loc.1 - 1),
			R270 => (loc.1, segsiz - loc.0 - 1),
		}
	}
	fn rot_right(&self) -> Rotation {
		match self{
			R0 => R90,
			R90 => R180,
			R180 => R270,
			R270 => R0,
		}
	}
	fn rot_left(&self) -> Rotation {
		match self{
			R180 => R90,
			R270 => R180,
			R0 => R270,
			R90 => R0,
		}
	}
	fn calc(from: CubeFace, to: CubeFace) -> Rotation {
		let identity = || panic!("Identity {:?} {:?}", from, to);
		let no = || panic!("No {:?} {:?}", from, to);
		match (from, to) {
			(Top, Top) => identity(),
			(Top, Up) => R180,
			(Top, Down) => R0,
			(Top, Left) => R90,
			(Top, Right) => R270,
			(Top, Bottom) => no(),
			(Down, Down) => identity(),
			(Down, Top) => R0,
			(Down, Bottom) => R0,
			(Down, Left) => R0,
			(Down, Right) => R0,
			(Down, Up) => no(),
			(Left, Left) => identity(),
			(Left, Top) => R270,
			(Left, Bottom) => R90,
			(Left, Up) => R0,
			(Left, Down) => R0,
			(Left, Right) => no(),
			(Right, Right) => identity(),
			(Right, Top) => R90,
			(Right, Bottom) => R270,
			(Right, Down) => R0,
			(Right, Up) => R0,
			(Up, Up) => identity(),
			(Up, Top) => R180,
			(Up, Bottom) => R180,
			(Up, Right) => R0,
			(Up, Left) => R0,
			(Up, Down) => no(),
		}
	}
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
enum CubeFace {
	Top,
	Bottom,
	Up,
	Down,
	Left,
	Right
}
use CubeFace::*;
impl CubeFace {
	fn to_up(&self) -> CubeFace {
		match self {
			Top => Up,
			Bottom => Down,
			Up => Top,
			Down => Top,
			Left => Top,
			Right => Top,
		}
	}
	fn to_down(&self) -> CubeFace {
		match self {
			Top => Down,
			Bottom => Up,
			Up => Bottom,
			Down => Bottom,
			Left => Bottom,
			Right => Bottom,
		}
	}
	fn to_left(&self) -> CubeFace {
		match self {
			Top => Left,
			Bottom => Right,
			Left => Up,
			Up => Right,
			Right => Down,
			Down => Left,
		}
	}
	fn to_right(&self) -> CubeFace {
		match self{
			Top => Right,
			Bottom => Left,
			Left => Down,
			Down => Right,
			Right => Up,
			up => Left,
		}
	}
}
