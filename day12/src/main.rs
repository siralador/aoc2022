use std::path::PathBuf;
use std::mem::replace;
use std::io::{Read, Write};
use std::fs::File;
use std::collections::{VecDeque, HashSet, HashMap};
use regex::{Regex, Match};
use anyhow::{Result, Context, anyhow};

mod utils;
use utils::*;

fn main() {
	let input: String = {
		let input_path = std::env::args().nth(1).unwrap_or("input.txt".to_string());
		let mut input_file = File::open(input_path).expect("Failed to open the input file");
		let mut input = String::new();
		input_file.read_to_string(&mut input).expect("Failed to read the input");
		input = input.trim().to_string();
		input
	};

	let lines = input.lines().collect::<Vec<_>>();

	let mut start = None;
	let mut end = None;
	let cells = lines.iter().enumerate().map(|(y, row)| row.chars().enumerate().map(|(x, c)| {
		let c = if c == 'S' { start = Some((x, y)); 'a' }
			else if c == 'E' { end = Some((x, y)); 'z' }
			else { c };
		c as u8 - 'a' as u8
	}).vec()).vec();
	let start = start.unwrap();
	let end = end.unwrap();

	let cells = Grid::from_vec_of_rows(cells[0].len(), cells.len(), cells);
	let mut tiles = Grid::from_raw(cells.width(), cells.height(), vec![Tile::default(); cells.width() * cells.height()]);
	let mut queue: HashSet<GridLoc> = [end].into_iter().collect();
	while !queue.is_empty() {
		'tile: for step_to in replace(&mut queue, HashSet::new()) {
			let mut yes = false;
			for step_from in cells.neighbors(step_to) {
				if tiles[step_from].step_to.contains(&step_to) { continue }
				if cells[step_from] + 1 == cells[step_to] {
					tiles[step_from].step_to.push(step_to);
					queue.insert(step_from);
					yes = true;
				}
			}
			if yes { continue 'tile; }
			for step_from in cells.neighbors(step_to) {
				if tiles[step_from].step_to.contains(&step_to) { continue }
				if cells[step_from] + 1 > cells[step_to] {
					tiles[step_from].step_to.push(step_to);
					queue.insert(step_from);
				}
			}
		}
	}
	println!("Finished");

	let mut paths = vec![vec![start]];
	let mut visited = HashSet::<GridLoc>::new();
	'master: for _ in 0..1000 {
		for path in replace(&mut paths, Vec::new()) {
			for step_to in &tiles[*path.last().unwrap()].step_to {
				//if path.contains(step_to) { panic!("{:?} {:?}", path, step_to); }
				//if path.contains(step_to) { continue }
				if visited.contains(step_to) { continue }
				visited.insert(*step_to);
				if *step_to == end {
					println!("Finished {}", path.len());
					break 'master;
				}
				let mut new_path = path.clone();
				new_path.push(*step_to);
				paths.push(new_path);
			}
		}

		for y in 0..cells.height() {
			for x in 0..cells.width() {
				if paths.iter().filter_map(|path| path.last()).filter(|pos| **pos == (x, y)).next().is_some() {
					print!("{}", (cells[(x, y)] + 'a' as u8) as char);
				} else {
					print!(".");
				}
			}
			println!("");
		}
		println!("");
	}

	let starts = cells.iter().filter_map(|(loc, value)| if *value == 0 { Some(loc) } else { None }).vec();
	let mut paths = starts.iter().map(|loc| vec![*loc]).vec();
	let mut visited = HashSet::<GridLoc>::new();
	'master: for _ in 0..1000 {
		for path in replace(&mut paths, Vec::new()) {
			for step_to in &tiles[*path.last().unwrap()].step_to {
				//if path.contains(step_to) { panic!("{:?} {:?}", path, step_to); }
				//if path.contains(step_to) { continue }
				if visited.contains(step_to) { continue }
				visited.insert(*step_to);
				if *step_to == end {
					println!("Finished {}", path.len());
					break 'master;
				}
				let mut new_path = path.clone();
				new_path.push(*step_to);
				paths.push(new_path);
			}
		}

		for y in 0..cells.height() {
			for x in 0..cells.width() {
				if paths.iter().filter_map(|path| path.last()).filter(|pos| **pos == (x, y)).next().is_some() {
					print!("{}", (cells[(x, y)] + 'a' as u8) as char);
				} else {
					print!(".");
				}
			}
			println!("");
		}
		println!("");
	}
}

#[derive(Clone)]
struct Tile {
	step_to: Vec<GridLoc>,
}
impl Default for Tile {
	fn default() -> Self { Tile {
		step_to: Vec::new(),
	} }
}

