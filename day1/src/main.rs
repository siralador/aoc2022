use std::io::{Read, Write};
use std::fs::File;

fn main() {
	let input: String = {
		let input_path = std::env::args().nth(1).unwrap_or("input.txt".to_string());
		let mut input_file = File::open(input_path).expect("Failed to open the input file");
		let mut input = String::new();
		input_file.read_to_string(&mut input).expect("Failed to read the input");
		input.trim().to_string()
	};

	let mut elves: Vec<Vec<usize>> = Vec::new();
	let mut elf = Vec::new();
	for line in input.lines() {
		let line = line.trim();
		if line.is_empty() {
			elves.push(elf);
			elf = Vec::new();
		} else {
			elf.push(line.parse().expect("numnum"));
		}
	}

	let max: Option<usize> = elves.iter().map(|elf| elf.iter().sum()).max();
	println!("Max: {:?}", max);

	let mut elf_sums: Vec<usize> = elves.iter().map(|elf| elf.iter().sum()).collect::<Vec<_>>();
	elf_sums.sort();
	elf_sums.reverse();
	println!("maxes: {} {} {}", elf_sums[0], elf_sums[1], elf_sums[2]);
	println!("Sum: {}", elf_sums[0] + elf_sums[1] + elf_sums[2]);
}
