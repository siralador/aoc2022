use std::io::{Read, Write};
use std::fs::File;
use std::collections::HashSet;

fn main() {
	let input: String = {
		let input_path = std::env::args().nth(1).unwrap_or("input.txt".to_string());
		let mut input_file = File::open(input_path).expect("Failed to open the input file");
		let mut input = String::new();
		input_file.read_to_string(&mut input).expect("Failed to read the input");
		input.trim().to_string()
	};

	let sacks = input.lines().map(|line| {
		assert!(line.len() % 2 == 0);
		let first = &line[..(line.len() / 2)];
		let second = &line[(line.len() / 2)..];
		(first, second)
	}).collect::<Vec<_>>();

	let sum: usize = sacks.iter().map(|(first, second)| {
		let mut first_contents = first.chars().collect::<HashSet<_>>();
		let mut second_contents = second.chars().collect::<HashSet<_>>();
		let mut sum = 0usize;
		for item in first_contents.drain() {
			if second_contents.remove(&item) {
				assert!(sum == 0);
				sum += priority(item) as usize;
			}
		}

		sum
	})
	.sum();

	println!("{}", sum);

	let lines = input.lines().collect::<Vec<_>>();
	let newsum: usize = lines.chunks(3).map(|group| {
		let a = group[0].chars().collect::<HashSet<_>>();
		let mut b = group[1].chars().collect::<HashSet<_>>();
		let mut c = group[2].chars().collect::<HashSet<_>>();
		let mut sum = 0usize;
		for item in a {
			let b = b.remove(&item);
			let c = c.remove(&item);
			if b && c {
				assert!(sum == 0);
				sum += priority(item) as usize;
			}
		}
		sum
	})
	.sum();
	println!("{}", newsum);
}

fn priority(c: char) -> u8 {
	match c {
		'a'..='z' => { (c as u8) - ('a' as u8) + 1 }
		'A'..='Z' => { (c as u8) - ('A' as u8) + 27 },
		_ => panic!("Unknown char {}", c)
	}
}
