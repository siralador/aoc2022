use std::ops::{Deref, DerefMut, Index, IndexMut};
use std::iter::{DoubleEndedIterator, IntoIterator, Iterator};
use anyhow::{Result, Context, anyhow};
use crate::utils::{lifetime_override_mut};

pub type GridLoc = (usize, usize);

pub struct GridBuffer<T> {
	width: usize,
	height: usize,
	data: Vec<T>,
}

pub struct Grid<T, B: Deref<Target = GridBuffer<T>>> {
	offset_x: usize,
	offset_y: usize,
	width: usize,
	height: usize,
	buf: B,
	_phantom: std::marker::PhantomData<T>,
}
impl<T> GridBuffer<T> {
	pub fn from_raw(width: usize, height: usize, data: Vec<T>) -> Self {
		assert!(data.len() == width * height);
		assert!(width != 0);
		assert!(height != 0);
		GridBuffer { width, height, data }
	}
	pub fn from_vec_of_rows(width: usize, height: usize, rows: Vec<Vec<T>>) -> Self {
		assert!(rows.len() == height);
		assert!(width != 0);
		assert!(height != 0);
		let mut data = Vec::new();
		for (i, row) in rows.into_iter().enumerate() {
			if row.len() != width { panic!("Row {} is not width {}", i, width); }
			data.extend(row);
		}
		GridBuffer { width, height, data }
	}
	pub fn grid(&self) -> Grid<T, &GridBuffer<T>> {
		Grid {
			offset_x: 0,
			offset_y: 0,
			width: self.width,
			height: self.height,
			buf: self,
			_phantom: Default::default(),
		}
	}
	pub fn grid_mut(&mut self) -> Grid<T, &mut GridBuffer<T>> {
		Grid {
			offset_x: 0,
			offset_y: 0,
			width: self.width,
			height: self.height,
			buf: self,
			_phantom: Default::default(),
		}
	}
}

impl<T, B: Deref<Target = GridBuffer<T>>> Grid<T, B> {
	fn loc_to_index(&self, (x, y): GridLoc) -> usize {
		self.width * (y + self.offset_y) + (x + self.offset_x)
	}
	pub fn width(&self) -> usize { self.width }
	pub fn height(&self) -> usize { self.height }
	pub fn neighbors(&self, (x, y): GridLoc) -> Vec<GridLoc> {
		let mut out = Vec::new();
		if let Some(x) = x.checked_sub(1) { out.push((x, y)); }
		if let Some(y) = y.checked_sub(1) { out.push((x, y)); }
		{
			let x = x + 1;
			if x < self.width { out.push((x, y)); }
		}
		{
			let y = y + 1;
			if y < self.height { out.push((x, y)); }
		}
		out
	}

	pub fn get(&self, (x, y): GridLoc) -> Result<&T> {
		if x >= self.width { Err(anyhow!("{} is beyond width {}", x, self.width)) }
		else if y >= self.height { Err(anyhow!("{} is beyond height {}", y, self.height)) }
		else { Ok(&self.buf.data[self.loc_to_index((x, y))]) }
	}
	pub fn col(&self, x: usize) -> Result<Column<'_, T, B>> {
		if x >= self.width { Err(anyhow!("{} is beyond width {}", x, self.width)) }
		else { Ok(Column { parent: self, x }) }
	}
	pub fn row(&self, y: usize) -> Result<Row<'_, T, B>> {
		if y >= self.height { Err(anyhow!("{} is beyond height {}", y, self.height)) }
		else { Ok(Row { parent: self, y })  }
	}
	pub fn cols(&self) -> ColsIterator<'_, T, B> { ColsIterator::new(self) }
	pub fn rows(&self) -> RowsIterator<'_, T, B> { RowsIterator::new(self) }
	pub fn iter(&self) -> GridIterator<'_, T, B, GridLocIterator> { GridIterator { iterator: GridLocIterator::new((0, 0), (self.width, self.height)), grid: self } }
}
impl<T, B: DerefMut<Target = GridBuffer<T>>> Grid<T, B> {
	pub fn get_mut(&mut self, (x, y): GridLoc) -> Result<&mut T> {
		if x >= self.width { Err(anyhow!("{} is beyond width {}", x, self.width)) }
		else if y >= self.height { Err(anyhow!("{} is beyond height {}", y, self.height)) }
		else {
			let index = self.loc_to_index((x, y));
			Ok(&mut self.buf.data[index])
		}
	}
	pub fn col_mut(&mut self, x: usize) -> Result<ColumnMut<'_, T, B>> {
		if x >= self.width { Err(anyhow!("{} is beyond width {}", x, self.width)) }
		else { Ok(ColumnMut { parent: self, x }) }
	}
	pub fn row_mut(&mut self, y: usize) -> Result<RowMut<'_, T, B>> {
		if y >= self.height { Err(anyhow!("{} is beyond height {}", y, self.height)) }
		else { Ok(RowMut { parent: self, y })  }
	}
	pub fn cols_mut(&mut self) -> ColsMutIterator<'_, T, B> { ColsMutIterator::new(self) }
	pub fn rows_mut(&mut self) -> RowsMutIterator<'_, T, B> { RowsMutIterator::new(self) }

}


impl<T, B: Deref<Target = GridBuffer<T>>> Index<GridLoc> for Grid<T, B> {
	type Output = T;
	fn index(&self, loc: GridLoc) -> &T { self.get(loc).unwrap() }
}
impl<T, B: DerefMut<Target = GridBuffer<T>>> IndexMut<GridLoc> for Grid<T, B> {
	fn index_mut(&mut self, loc: GridLoc) -> &mut T { self.get_mut(loc).unwrap() }
}

pub struct GridLocIterator {
	start: GridLoc,
	until: GridLoc,
	current_beginning: GridLoc,
	next_end: GridLoc,
	exhausted: bool,
}
impl GridLocIterator {
	fn new(start: GridLoc, until: GridLoc) -> Self { Self {
		start, until,
		current_beginning: start,
		next_end: until,
		exhausted: false,
	} }
}
impl Iterator for GridLocIterator {
	type Item = GridLoc;
	fn next(&mut self) -> Option<GridLoc> {
		if self.current_beginning.1 > self.next_end.1 ||
			(self.current_beginning.1 == self.next_end.1 && self.current_beginning.0 >= self.next_end.0)
		{
			self.exhausted = true;
		}
		if self.exhausted { return None };

		let ret = self.current_beginning;
		self.current_beginning.0 += 1;
		if self.current_beginning.0 >= self.until.0 {
			self.current_beginning.0 = self.start.0;
			self.current_beginning.1 += 1;
			if self.current_beginning.1 >= self.until.1 {
				self.exhausted = true;
			}
		}
		Some(ret)
	}
}
impl DoubleEndedIterator for GridLocIterator {
	fn next_back(&mut self) -> Option<Self::Item> {
		if self.current_beginning.1 > self.next_end.1 ||
			(self.current_beginning.1 == self.next_end.1 && self.current_beginning.0 >= self.next_end.0)
		{
			self.exhausted = true;
		}
		if self.exhausted { return None };

		let needs_relocate = if let Some(x) = self.next_end.0.checked_sub(1) {
			if x >= self.start.0 {
				self.next_end = (x, self.next_end.1);
				false
			}
			else { true }
		}
		else { true };
		if needs_relocate {
			let exhausted = if let Some(y) = self.next_end.1.checked_sub(1) {
				if y >= self.start.1 {
					self.next_end = (self.until.0 - 1, y);
					false
				}
				else { true }
			}
			else { true };
			if exhausted {
				self.exhausted = true;
				return None;
			}
		}
		Some(self.next_end)
	}
}
impl From<(GridLoc, GridLoc)> for GridLocIterator {
	fn from((start, until): (GridLoc, GridLoc)) -> Self { Self::new(start, until) }
}

pub struct GridIterator<'g, T, B: Deref<Target = GridBuffer<T>>, I> {
	iterator: I,
	grid: &'g Grid<T, B>,
}
impl<'g, T, B: Deref<Target = GridBuffer<T>>, I: Iterator<Item = GridLoc>> Iterator for GridIterator<'g, T, B, I> {
	type Item = (GridLoc, &'g T);
	fn next(&mut self) -> Option<Self::Item> { self.iterator.next().map(|loc| (loc, &self.grid[loc])) }
}
pub struct GridMutIterator<'g, T, B: DerefMut<Target = GridBuffer<T>>, I> {
	iterator: I,
	grid: &'g mut Grid<T, B>,
}
impl<'g, T, B: DerefMut<Target = GridBuffer<T>>, I: Iterator<Item = GridLoc>> Iterator for GridMutIterator<'g, T, B, I> {
	type Item = (GridLoc, &'g mut T);
	fn next(&mut self) -> Option<Self::Item> { self.iterator.next().map(|loc| (loc, unsafe { lifetime_override_mut(&mut self.grid[loc]) })) }
}
pub struct GridIntoIter<T> {
	iterator: GridLocIterator,
	items: std::vec::IntoIter<T>,
}
impl<T> Iterator for GridIntoIter<T> {
	type Item = (GridLoc, T);
	fn next(&mut self) -> Option<Self::Item> { self.items.next().map(|item| (self.iterator.next().unwrap(), item)) }
}
impl<T> IntoIterator for GridBuffer<T> {
	type IntoIter = GridIntoIter<T>;
	type Item = ((usize, usize), T);
	fn into_iter(self) -> Self::IntoIter { GridIntoIter { iterator: GridLocIterator::new((0, 0), (self.width, self.height)), items: self.data.into_iter() } }
}


pub struct Row<'g, T, B: Deref<Target = GridBuffer<T>>> {
	parent: &'g Grid<T, B>,
	y: usize,
}
impl<'g, T, B: Deref<Target = GridBuffer<T>>> Row<'g, T, B> {
	pub fn get(&self, x: usize) -> Result<&T> { self.parent.get((x, self.y)) }
	pub fn y(&self) -> usize { self.y }
	pub fn iter(&self) -> GridIterator<'g, T, B, GridLocIterator> { GridIterator { iterator: GridLocIterator::new((0, self.y), (self.parent.width, self.y)), grid: self.parent } }
}
impl<'g, T, B: Deref<Target = GridBuffer<T>>> IntoIterator for Row<'g, T, B> {
	type Item = ((usize, usize), &'g T);
	type IntoIter = GridIterator<'g, T, B, GridLocIterator>;
	fn into_iter(self) -> Self::IntoIter { self.iter() }
}
pub struct RowMut<'g, T, B: DerefMut<Target = GridBuffer<T>>> {
	parent: &'g mut Grid<T, B>,
	y: usize,
}
impl<'g, T, B: DerefMut<Target = GridBuffer<T>>> RowMut<'g, T, B> {
	pub fn get(&self, x: usize) -> Result<&T> { self.parent.get((x, self.y)) }
	pub fn get_mut(&mut self, x: usize) -> Result<&mut T> { self.parent.get_mut((x, self.y)) }
	pub fn y(&self) -> usize { self.y }
	pub fn range(&self) -> (GridLoc, GridLoc) { ((0, self.y), (self.parent.width, self.y+1)) }
	pub fn as_row(&self) -> Row<'_, T, B> { Row { parent: self.parent, y: self.y } }
	pub fn iter(&self) -> GridIterator<'_, T, B, GridLocIterator> { GridIterator { iterator: self.range().into(), grid: self.parent } }
	pub fn iter_mut(&mut self) -> GridMutIterator<'_, T, B, GridLocIterator> { GridMutIterator { iterator: self.range().into(), grid: self.parent } }
}
impl<'g, T, B: DerefMut<Target = GridBuffer<T>>> IntoIterator for RowMut<'g, T, B> {
	type Item = ((usize, usize), &'g mut T);
	type IntoIter = GridMutIterator<'g, T, B, GridLocIterator>;
	fn into_iter(self) -> Self::IntoIter { GridMutIterator { iterator: GridLocIterator::new((0, self.y), (self.parent.width, self.y)), grid: self.parent } }
}
pub struct RowsIterator<'g, T, B: Deref<Target = GridBuffer<T>>> {
	parent: &'g Grid<T, B>,
	current_beginning: usize,
	next_end: usize,
	exhausted: bool,
}
impl<'g, T, B: Deref<Target = GridBuffer<T>>> RowsIterator<'g, T, B> {
	fn new(parent: &'g Grid<T, B>) -> Self { Self {
		parent,
		current_beginning: 0,
		next_end: parent.width,
		exhausted: false,
	} }
}
impl<'g, T, B: Deref<Target = GridBuffer<T>>> Iterator for RowsIterator<'g, T, B> {
	type Item = Row<'g, T, B>;
	fn next(&mut self) -> Option<Self::Item> {
		if self.current_beginning >= self.next_end { self.exhausted = true; }
		if self.exhausted { return None }
		let ret_i = self.current_beginning;
		self.current_beginning += 1;
		if self.current_beginning >= self.parent.width { self.exhausted = true; }
		Some(Row { parent: self.parent, y: ret_i })
	}
}
pub struct RowsMutIterator<'g, T, B: DerefMut<Target = GridBuffer<T>>> {
	parent: &'g mut Grid<T, B>,
	current_beginning: usize,
	next_end: usize,
	exhausted: bool,
}
impl<'g, T, B: DerefMut<Target = GridBuffer<T>>> RowsMutIterator<'g, T, B> {
	fn new(parent: &'g mut Grid<T, B>) -> Self { Self {
		next_end: parent.width,
		parent,
		current_beginning: 0,
		exhausted: false,
	} }
}
impl<'g, T, B: DerefMut<Target = GridBuffer<T>>> Iterator for RowsMutIterator<'g, T, B> {
	type Item = RowMut<'g, T, B>;
	fn next(&mut self) -> Option<Self::Item> {
		if self.current_beginning >= self.next_end { self.exhausted = true; }
		if self.exhausted { return None }
		let ret_i = self.current_beginning;
		self.current_beginning += 1;
		if self.current_beginning >= self.parent.width { self.exhausted = true; }
		Some(RowMut { parent: unsafe { lifetime_override_mut(self.parent) }, y: ret_i })
	}
}

pub struct Column<'g, T, B: Deref<Target = GridBuffer<T>>> {
	parent: &'g Grid<T, B>,
	x: usize,
}
impl<'g, T, B: Deref<Target = GridBuffer<T>>> Column<'g, T, B> {
	pub fn get(&self, y: usize) -> Result<&T> { self.parent.get((self.x, y)) }
	pub fn x(&self) -> usize { self.x }
	pub fn range(&self) -> (GridLoc, GridLoc) { ((self.x, 0), (self.x+1, self.parent.height)) }
	pub fn iter(&self) -> GridIterator<'g, T, B, GridLocIterator> { GridIterator { iterator: GridLocIterator::new((self.x, 0), (self.x, self.parent.height)), grid: self.parent } } }
impl<'g, T, B: Deref<Target = GridBuffer<T>>> IntoIterator for Column<'g, T, B> {
	type Item = ((usize, usize), &'g T);
	type IntoIter = GridIterator<'g, T, B, GridLocIterator>;
	fn into_iter(self) -> Self::IntoIter { self.iter() }
}
pub struct ColumnMut<'g, T, B: DerefMut<Target = GridBuffer<T>>> {
	parent: &'g mut Grid<T, B>,
	x: usize,
}
impl<'g, T, B: DerefMut<Target = GridBuffer<T>>> ColumnMut<'g, T, B> {
	pub fn get(&self, y: usize) -> Result<&T> { self.parent.get((self.x, y)) }
	pub fn get_mut(&mut self, y: usize) -> Result<&mut T> { self.parent.get_mut((self.x, y)) }
	pub fn x(&self) -> usize { self.x }
	pub fn range(&self) -> (GridLoc, GridLoc) { ((self.x, 0), (self.x+1, self.parent.height)) }
	pub fn as_column(&self) -> Column<'_, T, B> { Column { parent: self.parent, x: self.x } }
	pub fn iter(&self) -> GridIterator<'_, T, B, GridLocIterator> { GridIterator { iterator: GridLocIterator::new((self.x, 0), (self.x+1, self.parent.height)), grid: self.parent } }
	pub fn iter_mut(&mut self) -> GridMutIterator<'_, T, B, GridLocIterator> { GridMutIterator { iterator: self.range().into(), grid: self.parent } }
}
impl<'g, T, B: DerefMut<Target = GridBuffer<T>>> IntoIterator for ColumnMut<'g, T, B> {
	type Item = ((usize, usize), &'g mut T);
	type IntoIter = GridMutIterator<'g, T, B, GridLocIterator>;
	fn into_iter(self) -> Self::IntoIter { GridMutIterator { iterator: self.range().into(), grid: self.parent } }
}
pub struct ColsIterator<'g, T, B: Deref<Target = GridBuffer<T>>> {
	parent: &'g Grid<T, B>,
	current_beginning: usize,
	next_end: usize,
	exhausted: bool,
}
impl<'g, T, B: Deref<Target = GridBuffer<T>>> ColsIterator<'g, T, B> {
	fn new(parent: &'g Grid<T, B>) -> Self { Self {
		parent,
		current_beginning: 0,
		next_end: parent.width,
		exhausted: false,
	} }
}
impl<'g, T, B: Deref<Target = GridBuffer<T>>> Iterator for ColsIterator<'g, T, B> {
	type Item = Column<'g, T, B>;
	fn next(&mut self) -> Option<Self::Item> {
		if self.current_beginning >= self.next_end { self.exhausted = true; }
		if self.exhausted { return None }
		let ret_i = self.current_beginning;
		self.current_beginning += 1;
		if self.current_beginning >= self.parent.width { self.exhausted = true; }
		Some(Column { parent: self.parent, x: ret_i })
	}
}
pub struct ColsMutIterator<'g, T, B: DerefMut<Target = GridBuffer<T>>> {
	parent: &'g mut Grid<T, B>,
	current_beginning: usize,
	next_end: usize,
	exhausted: bool,
}
impl<'g, T, B: DerefMut<Target = GridBuffer<T>>> ColsMutIterator<'g, T, B> {
	fn new(parent: &'g mut Grid<T, B>) -> Self { Self {
		next_end: parent.width,
		parent,
		current_beginning: 0,
		exhausted: false,
	} }
}
impl<'g, T, B: DerefMut<Target = GridBuffer<T>>> Iterator for ColsMutIterator<'g, T, B> {
	type Item = ColumnMut<'g, T, B>;
	fn next(&mut self) -> Option<Self::Item> {
		if self.current_beginning >= self.next_end { self.exhausted = true; }
		if self.exhausted { return None }
		let ret_i = self.current_beginning;
		self.current_beginning += 1;
		if self.current_beginning >= self.parent.width { self.exhausted = true; }
		Some(ColumnMut { parent: unsafe { lifetime_override_mut(self.parent) }, x: ret_i })
	}
}
