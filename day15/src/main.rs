use std::path::PathBuf;
use std::mem::replace;
use std::io::{Read, Write};
use std::fs::File;
use std::collections::{VecDeque, HashSet, HashMap};
use regex::{Regex, Match};
use anyhow::{Result, Context, anyhow};
use std::str::Chars;
use std::iter::Peekable;
use std::cmp::Ordering;

mod utils;
use utils::*;

fn main() {
	let input: String = {
		let input_path = std::env::args().nth(1).unwrap_or("input.txt".to_string());
		let mut input_file = File::open(input_path).expect("Failed to open the input file");
		let mut input = String::new();
		input_file.read_to_string(&mut input).expect("Failed to read the input");
		input = input.trim().to_string();
		input
	};

	let lines = input.lines().collect::<Vec<_>>();

	let mut min_x = 0;
	let mut max_x = 0;
	let sensors = lines.iter().map(|line| {
		println!("{}", line);
		let bits = line.split(" ").vec();
		let sensor_x = &bits[2][2..];
		let sensor_x: isize = sensor_x[0..sensor_x.len()-1].parse().unwrap();
		let sensor_y = &bits[3][2..];
		let sensor_y: isize = sensor_y[0..sensor_y.len()-1].parse().unwrap();
		let beacon_x = &bits[8][2..];
		let beacon_x: isize = beacon_x[0..beacon_x.len()-1].parse().unwrap();
		let beacon_y = &bits[9][2..];
		let beacon_y: isize = beacon_y[0..beacon_y.len()-0].parse().unwrap();
		let pos = (sensor_x, sensor_y);
		let beacon = (beacon_x, beacon_y);
		let distance = manhattan(pos, beacon);
		min_x = min_x.min(pos.0 - distance);
		max_x = max_x.max(pos.0 + distance);
		Sensor {
			pos, beacon, distance,
		}
	}).vec();

	let y = 2000000;
	let mut cannot_be_count = 0;
	println!("{}", min_x);
	let min_x = min_x - 10;
	let max_x = max_x + 10;
	/*for x in min_x..=max_x {
		let pos = (x, y);
		if let Some(sensor) = sensors.iter().find(|sensor| manhattan(pos, sensor.pos) <= sensor.distance) {
			if pos != sensor.beacon {
				cannot_be_count += 1;
			}
			assert!(pos != sensor.pos);
		} else {
		}
	}*/
	println!("");
	println!("{}", cannot_be_count);

	let mut y = 0;
	let mut x = 0;
	let max = 4000000;
	let mut seen_sensors: HashSet<usize> = HashSet::new();
	while y <= max {
		while x <= max {
			let pos = (x, y);
			if let Some((i, sensor)) = sensors.iter().enumerate().find(|(i, sensor)| manhattan(pos, sensor.pos) <= sensor.distance && !seen_sensors.contains(i)) {
				x = sensor.pos.0 + (sensor.distance - (sensor.pos.1 - pos.1).abs()) + 1;
				seen_sensors.insert(i);
				if x > max {
					x = 0;
					y += 1;
					seen_sensors = HashSet::new();
				}
			} else {
				println!("Found at {} {}; {}", x, y, x * 4000000 + y);
				println!("Found at {} {}; {}", x, y, x * 4000000 + y);
				println!("Found at {} {}; {}", x, y, x * 4000000 + y);
				println!("Found at {} {}; {}", x, y, x * 4000000 + y);
				println!("Found at {} {}; {}", x, y, x * 4000000 + y);
				println!("Found at {} {}; {}", x, y, x * 4000000 + y);
				return;
			}
			if y % 100 == 0 { println!("y: {}", y); }
			 //println!("y: {}", y);
		}
		println!("dumb at {} {}; {}", x, y, x * 4000000 + y);
	}
}

type Loc = (isize, isize);

struct Sensor {
	pos: Loc,
	beacon: Loc,
	distance: isize,
}

fn manhattan(loc1: Loc, loc2: Loc) -> isize {
	return (loc1.0 - loc2.0).abs() + (loc1.1 - loc2.1).abs()
}
