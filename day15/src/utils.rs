

pub trait IteratorExt: Iterator {
	fn vec(self) -> Vec<Self::Item>;
}

impl<O, I: Iterator<Item = O>> IteratorExt for I {
	fn vec(self) -> Vec<Self::Item> { self.collect() }
}


unsafe fn lifetime_override_mut<'a, 'b, T>(ptr: &'a mut T) -> &'b mut T { std::mem::transmute(ptr) }

mod grid;
pub use grid::*;

pub trait MyAsRef<T> {
	fn as_ref(&self) -> &T;
}
pub trait MyAsMut<T>: MyAsRef<T> {
	fn as_mut(&mut self) -> &mut T;
}
impl<T> MyAsRef<T> for &T {
	fn as_ref(&self) -> &T { self }
}
impl<T> MyAsRef<T> for &mut T {
	fn as_ref(&self) -> &T { self }
}
impl<T> MyAsMut<T> for &mut T {
	fn as_mut(&mut self) -> &mut T { self }
}
