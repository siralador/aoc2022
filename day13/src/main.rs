use std::path::PathBuf;
use std::mem::replace;
use std::io::{Read, Write};
use std::fs::File;
use std::collections::{VecDeque, HashSet, HashMap};
use regex::{Regex, Match};
use anyhow::{Result, Context, anyhow};
use std::str::Chars;
use std::iter::Peekable;
use std::cmp::Ordering;

mod utils;
use utils::*;

fn main() {
	let input: String = {
		let input_path = std::env::args().nth(1).unwrap_or("input.txt".to_string());
		let mut input_file = File::open(input_path).expect("Failed to open the input file");
		let mut input = String::new();
		input_file.read_to_string(&mut input).expect("Failed to read the input");
		input = input.trim().to_string();
		input
	};

	let lines = input.lines().collect::<Vec<_>>();

	let lines = lines.into_iter().map(|line| line.chars().filter(|c| !c.is_whitespace()).collect::<String>()).vec();
	let mut packets = Vec::new();
	let mut into_packets = lines.iter();
	while let Some(a) = into_packets.next() {
		packets.push(parse_packet(&mut a.chars().peekable()));
		let b = into_packets.next().unwrap();
		packets.push(parse_packet(&mut b.chars().peekable()));
		into_packets.next();
	}

	let mut comparisons = packets.iter();
	let mut score = 0;
	let mut index = 1;
	while let Some(left) = comparisons.next() {
		let right = comparisons.next().unwrap();
		if left < right { score += index; }
		index += 1;
	}
	println!("{}", score);

	let divider_1 = ListItem::List(vec![ListItem::List(vec![ListItem::Num(2)])]);
	packets.push(divider_1.clone());
	let divider_2 = ListItem::List(vec![ListItem::List(vec![ListItem::Num(6)])]);
	packets.push(divider_2.clone());
	packets.sort();
	let (divider_1_i, _) = packets.iter().enumerate().find(|(_, packet)| **packet == divider_1).unwrap();
	let divider_1_i = divider_1_i + 1;
	let (divider_2_i, _) = packets.iter().enumerate().find(|(_, packet)| **packet == divider_2).unwrap();
	let divider_2_i = divider_2_i + 1;
	println!("{} {} {}", divider_1_i, divider_2_i, divider_1_i * divider_2_i);
}

fn parse_packet(packet: &mut Peekable<Chars>) -> ListItem {
	let next = packet.next().unwrap();
	if next == '[' {
		let mut list = Vec::new();
		loop {
			let next = packet.peek().unwrap();
			if *next == ']' { packet.next(); break; }
			else if *next == ',' { packet.next(); }
			else { list.push(parse_packet(packet)); }
		};
		ListItem::List(list)
	} else {
		let mut num = String::new();
		num.push(next);
		while *packet.peek().unwrap() != ']' && *packet.peek().unwrap() != ',' {
			num.push(packet.next().unwrap());
		}
		ListItem::Num(num.parse().expect(&num))
	}
}

#[derive(Debug, Clone, PartialEq, Eq, Ord)]
enum ListItem {
	List(Vec<ListItem>),
	Num(i64),
}
impl PartialOrd for ListItem {
	fn partial_cmp(&self, right: &Self) -> Option<std::cmp::Ordering> {
		let left = self;
		use ListItem::*;
		match (left, right) {
			(Num(left), Num(right)) => { left.partial_cmp(right) },
			(List(left), List(right)) => {
				let mut left_iter = left.iter();
				let mut right_iter = right.iter();
				while let Some(left) = left_iter.next() {
					let Some(right) = right_iter.next() else {
						return Some(std::cmp::Ordering::Greater);
					};
					match left.partial_cmp(right) {
						Some(Ordering::Equal) => {},
						other => return other,
					}
				}
				if right_iter.next().is_none() { Some(Ordering::Equal) }
				else { Some(Ordering::Less) }
			},
			(Num(left), List(_)) => { List(vec![Num(*left)]).partial_cmp(right) },
			(List(_), Num(right)) => { left.partial_cmp(&List(vec![Num(*right)])) }
		}
	}
}
