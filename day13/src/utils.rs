use std::ops::{Index, IndexMut};
use std::iter::{IntoIterator, Iterator};
use anyhow::{Result, Context, anyhow};


pub trait IteratorExt: Iterator {
	fn vec(self) -> Vec<Self::Item>;
}

impl<O, I: Iterator<Item = O>> IteratorExt for I {
	fn vec(self) -> Vec<Self::Item> { self.collect() }
}


unsafe fn lifetime_override_mut<'a, 'b, T>(ptr: &'a mut T) -> &'b mut T { std::mem::transmute(ptr) }

pub type GridLoc = (usize, usize);
pub struct Grid<T> {
	width: usize,
	height: usize,
	data: Vec<T>,
}
impl<T> Grid<T> {
	pub fn from_raw(width: usize, height: usize, data: Vec<T>) -> Self {
		assert!(data.len() == width * height);
		Grid { width, height, data }
	}
	pub fn from_vec_of_rows(width: usize, height: usize, rows: Vec<Vec<T>>) -> Self {
		assert!(rows.len() == height);
		let mut data = Vec::new();
		for (i, row) in rows.into_iter().enumerate() {
			if row.len() != width { panic!("Row {} is not width {}", i, width); }
			data.extend(row);
		}
		Grid { width, height, data }
	}

	fn loc_to_index(&self, (x, y): GridLoc) -> usize {
		self.width * y + x
	}
	pub fn width(&self) -> usize { self.width }
	pub fn height(&self) -> usize { self.height }
	pub fn neighbors(&self, (x, y): GridLoc) -> Vec<GridLoc> {
		let mut out = Vec::new();
		if let Some(x) = x.checked_sub(1) { out.push((x, y)); }
		if let Some(y) = y.checked_sub(1) { out.push((x, y)); }
		{
			let x = x + 1;
			if x < self.width { out.push((x, y)); }
		}
		{
			let y = y + 1;
			if y < self.height { out.push((x, y)); }
		}
		out
	}

	pub fn get(&self, (x, y): GridLoc) -> Result<&T> {
		if x > self.width { Err(anyhow!("{} is beyond width {}", x, self.width)) }
		else if y > self.height { Err(anyhow!("{} is beyond height {}", y, self.height)) }
		else { Ok(&self.data[self.loc_to_index((x, y))]) }
	}
	pub fn get_mut(&mut self, (x, y): GridLoc) -> Result<&mut T> {
		if x > self.width { Err(anyhow!("{} is beyond width {}", x, self.width)) }
		else if y > self.height { Err(anyhow!("{} is beyond height {}", y, self.height)) }
		else {
			let index = self.loc_to_index((x, y));
			Ok(&mut self.data[index])
		}
	}
	/*pub fn col(&self, x: usize) -> Result<Column<'_, T>> {
		if 

	}*/

	pub fn iter(&self) -> GridIterator<'_, T, GridLocIterator> { GridIterator { iterator: GridLocIterator::new((0, 0), (self.width, self.height)), grid: self } }
}
impl<T> Index<GridLoc> for Grid<T> {
	type Output = T;
	fn index(&self, loc: GridLoc) -> &T { self.get(loc).unwrap() }
}
impl<T> IndexMut<GridLoc> for Grid<T> {
	fn index_mut(&mut self, loc: GridLoc) -> &mut T { self.get_mut(loc).unwrap() }
}

pub struct GridLocIterator {
	start: GridLoc,
	until: GridLoc,
	current: GridLoc,
}
impl GridLocIterator {
	fn new(start: GridLoc, until: GridLoc) -> Self { Self {
		start, until,
		current: start,
	} }
}
impl Iterator for GridLocIterator {
	type Item = GridLoc;
	fn next(&mut self) -> Option<GridLoc> {
		let ret = self.current;
		self.current.0 += 1;
		if self.current.0 >= self.until.0 {
			self.current.0 = self.start.0;
			self.current.1 += 1;
			if self.current.1 >= self.until.1 {
				return None
			}
		}
		Some(ret)
	}
}
impl From<(GridLoc, GridLoc)> for GridLocIterator {
	fn from((start, until): (GridLoc, GridLoc)) -> Self { Self::new(start, until) }
}

pub struct GridIterator<'g, T, I> {
	iterator: I,
	grid: &'g Grid<T>,
}
impl<'g, T, I: Iterator<Item = GridLoc>> Iterator for GridIterator<'g, T, I> {
	type Item = (GridLoc, &'g T);
	fn next(&mut self) -> Option<Self::Item> { self.iterator.next().map(|loc| (loc, &self.grid[loc])) }
}
pub struct GridMutIterator<'g, T, I> {
	iterator: I,
	grid: &'g mut Grid<T>,
}
impl<'g, T, I: Iterator<Item = GridLoc>> Iterator for GridMutIterator<'g, T, I> {
	type Item = (GridLoc, &'g mut T);
	fn next(&mut self) -> Option<Self::Item> { self.iterator.next().map(|loc| (loc, unsafe { lifetime_override_mut(&mut self.grid[loc]) })) }
}
pub struct GridIntoIter<T> {
	iterator: GridLocIterator,
	items: std::vec::IntoIter<T>,
}
impl<T> Iterator for GridIntoIter<T> {
	type Item = (GridLoc, T);
	fn next(&mut self) -> Option<Self::Item> { self.items.next().map(|item| (self.iterator.next().unwrap(), item)) }
}
impl<T> IntoIterator for Grid<T> {
	type IntoIter = GridIntoIter<T>;
	type Item = ((usize, usize), T);
	fn into_iter(self) -> Self::IntoIter { GridIntoIter { iterator: GridLocIterator::new((0, 0), (self.width, self.height)), items: self.data.into_iter() } }
}


pub struct Row<'g, T> {
	parent: &'g Grid<T>,
	y: usize,
}
impl<'g, T> Row<'g, T> {
	pub fn get(&self, x: usize) -> Result<&T> { self.parent.get((x, self.y)) }
	pub fn y(&self) -> usize { self.y }
	pub fn iter(&self) -> GridIterator<'g, T, GridLocIterator> { GridIterator { iterator: GridLocIterator::new((0, self.y), (self.parent.width, self.y)), grid: self.parent } }
}
impl<'g, T> IntoIterator for Row<'g, T> {
	type Item = ((usize, usize), &'g T);
	type IntoIter = GridIterator<'g, T, GridLocIterator>;
	fn into_iter(self) -> Self::IntoIter { self.iter() }
}
pub struct RowMut<'g, T> {
	parent: &'g mut Grid<T>,
	y: usize,
}
impl<'g, T> RowMut<'g, T> {
	pub fn get(&self, x: usize) -> Result<&T> { self.parent.get((x, self.y)) }
	pub fn get_mut(&mut self, x: usize) -> Result<&mut T> { self.parent.get_mut((x, self.y)) }
	pub fn y(&self) -> usize { self.y }
	pub fn range(&self) -> (GridLoc, GridLoc) { ((0, self.y), (self.parent.width, self.y+1)) }
	pub fn as_row(&self) -> Row<'_, T> { Row { parent: self.parent, y: self.y } }
	pub fn iter(&self) -> GridIterator<'_, T, GridLocIterator> { GridIterator { iterator: self.range().into(), grid: self.parent } }
	pub fn iter_mut(&mut self) -> GridMutIterator<'_, T, GridLocIterator> { GridMutIterator { iterator: self.range().into(), grid: self.parent } }
}
impl<'g, T> IntoIterator for RowMut<'g, T> {
	type Item = ((usize, usize), &'g mut T);
	type IntoIter = GridMutIterator<'g, T, GridLocIterator>;
	fn into_iter(self) -> Self::IntoIter { GridMutIterator { iterator: GridLocIterator::new((0, self.y), (self.parent.width, self.y)), grid: self.parent } }
}

pub struct Column<'g, T> {
	parent: &'g Grid<T>,
	x: usize,
}
impl<'g, T> Column<'g, T> {
	pub fn get(&self, y: usize) -> Result<&T> { self.parent.get((self.x, y)) }
	pub fn x(&self) -> usize { self.x }
	pub fn range(&self) -> (GridLoc, GridLoc) { ((self.x, 0), (self.x+1, self.parent.height)) }
	pub fn iter(&self) -> GridIterator<'g, T, GridLocIterator> { GridIterator { iterator: GridLocIterator::new((self.x, 0), (self.x, self.parent.height)), grid: self.parent } }
}
impl<'g, T> IntoIterator for Column<'g, T> {
	type Item = ((usize, usize), &'g T);
	type IntoIter = GridIterator<'g, T, GridLocIterator>;
	fn into_iter(self) -> Self::IntoIter { self.iter() }
}
pub struct ColumnMut<'g, T> {
	parent: &'g mut Grid<T>,
	x: usize,
}
impl<'g, T> ColumnMut<'g, T> {
	pub fn get(&self, y: usize) -> Result<&T> { self.parent.get((self.x, y)) }
	pub fn get_mut(&mut self, y: usize) -> Result<&mut T> { self.parent.get_mut((self.x, y)) }
	pub fn x(&self) -> usize { self.x }
	pub fn range(&self) -> (GridLoc, GridLoc) { ((self.x, 0), (self.x+1, self.parent.height)) }
	pub fn as_oclumn(&self) -> Column<'_, T> { Column { parent: self.parent, x: self.x } }
	pub fn iter(&self) -> GridIterator<'_, T, GridLocIterator> { GridIterator { iterator: GridLocIterator::new((self.x, 0), (self.x+1, self.parent.height)), grid: self.parent } }
	pub fn iter_mut(&mut self) -> GridMutIterator<'_, T, GridLocIterator> { GridMutIterator { iterator: self.range().into(), grid: self.parent } }
}
impl<'g, T> IntoIterator for ColumnMut<'g, T> {
	type Item = ((usize, usize), &'g mut T);
	type IntoIter = GridMutIterator<'g, T, GridLocIterator>;
	fn into_iter(self) -> Self::IntoIter { GridMutIterator { iterator: self.range().into(), grid: self.parent } }
}
