use std::path::PathBuf;
use std::mem::replace;
use std::io::{Read, Write};
use std::fs::File;
use std::collections::{BTreeMap, VecDeque, HashSet, HashMap};
use regex::{Regex, Match};
use anyhow::{Result, Context, anyhow};
use std::str::Chars;
use std::iter::Peekable;
use std::cmp::Ordering;
use rayon::prelude::*;
use std::collections::BTreeSet;

mod utils;
use utils::*;

const ARENA_WIDTH: usize = 7;

fn main() {
	let input: String = {
		let input_path = std::env::args().nth(1).unwrap_or("input.txt".to_string());
		let mut input_file = File::open(input_path).expect("Failed to open the input file");
		let mut input = String::new();
		input_file.read_to_string(&mut input).expect("Failed to read the input");
		input = input.trim().to_string();
		input
	};

	let lines = input.lines().collect::<Vec<_>>();

	let blooprints = lines.iter().map(|line| {
		let colon = line.find(":").unwrap();
		let beginning = &line[0..colon];
		let id: usize = beginning.split(" ").last().unwrap().parse().unwrap();
		let after = &line[colon+1..];
		let sentences = after.split(".").vec();
		let blooprint = Blooprint {
			ore_robot_ore_cost: sentences[0].trim().split(" ").nth(4).unwrap().parse().unwrap(),
			clay_robot_ore_cost: sentences[1].trim().split(" ").nth(4).unwrap().parse().unwrap(),
			obsidian_robot_ore_cost: sentences[2].trim().split(" ").nth(4).unwrap().parse().unwrap(),
			obsidian_robot_clay_cost: sentences[2].trim().split(" ").nth(7).unwrap().parse().unwrap(),
			geode_robot_ore_cost: sentences[3].trim().split(" ").nth(4).unwrap().parse().unwrap(),
			geode_robot_obsidian_cost: sentences[3].trim().split(" ").nth(7).unwrap().parse().unwrap(),
		};
		(id, blooprint)
	}).to_map();

	let scores: BTreeMap<usize, (usize, usize)> = blooprints.iter().map(|(id, blooprint)| {
		let mut nodes: BTreeMap<Node, usize> = BTreeMap::new();
		let initial_robots = RobotArray { ore: 1, clay: 0, obsidian: 0, geode: 0 };
		nodes.insert(Node { robots: initial_robots.clone() }, 0);
		let mut states = vec![State {
			robots: initial_robots,
			inventory: Inventory::default(),
			purchased_on_minute: usize::MAX,
		}];
		for minute in 0..24 {
			let tasks = replace(&mut states, Vec::new());
			let new_states: BTreeSet<_> = (
				tasks.par_iter().map(|state| {
					let purchases = [Robot::Geode, Robot::Obsidian, Robot::Clay, Robot::Ore];
					let mut branches = purchases.iter().filter_map(|purchase| {
						let mut inventory = state.inventory.clone();
						if purchase.buy(&mut inventory, blooprint) {
							let robots = state.robots.clone();
							Some((State { inventory, robots, purchased_on_minute: minute }, Some(purchase)))
						}
						else { Some((state.clone(), None)) }
					}).vec();
					//Inaction branch:
					if branches.is_empty() { branches.push((state.clone(), None)); }

					//Move forward in time
					let new_states = branches.into_iter().map(|(mut branch, purchase)| {
						branch.robots.do_robots(&mut branch.inventory);
						if let Some(robot) = purchase { branch.robots.add(robot.clone()); }
						branch
					}).vec();

					new_states
				})
				.collect::<Vec<_>>().into_iter()
				.map(|v| v.into_iter())
				.flatten()
				.filter(|new_state| {
					if new_state.purchased_on_minute == minute {
						let node = Node { robots: new_state.robots.clone() };
						let previous_time = nodes.get(&node).cloned().unwrap_or(usize::MAX);
						if minute < previous_time { nodes.insert(node, minute); }
						minute <= previous_time
					}
					else { true }
				})
			).collect();
			states.extend(new_states.into_iter());
			
			let limit = 500000;
			if states.len() > limit {
				states.sort_by_key(|state| state.inventory.clone());
				states.reverse();
				states.drain(limit..);
			}
			println!("End of minute {}; {} states", minute + 1, states.len());
		}
		let best = states.iter().max_by_key(|state| state.inventory.geodes).unwrap();
		println!("Best state for blooprint {}: {:?}", id, best);
		let score = id * best.inventory.geodes;
		println!("Score for blooprint {}: {}", id, score);
		let brute_force_score = score;

		let priority = [Robot::Geode, Robot::Obsidian, Robot::Clay, Robot::Ore];
		let mut inventory = Inventory::default();
		let mut robots = vec![Robot::Ore];
		let max_robots = blooprint.max_robots_needed();

		println!("\n\n\nBegin blooprint {id}");
		println!("{:?}", blooprint);
		for minute in 0..24 {
			println!("\nBegin minute {minute}");
			println!(" - {:?}", RobotArray::calc(&robots));
			println!(" - {:?}", inventory);

			//Attempt to make a purchase
			let mut purchase = None;
			'purchase: for i in 0..priority.len() {
				let mut try_create = &priority[i];
				let mut higher_priority = priority[0..i].into_iter().vec();

				//if try_create != Robot::Ore && try_create.ore_cost(blooprint) >
				if *try_create == Robot::Clay && try_create.ore_cost(blooprint) >= Robot::Ore.ore_cost(blooprint) && RobotArray::calc(&robots).ore < max_robots.ore {
					println!(" - Force override Clay purchase attempt with Ore");
					try_create = &Robot::Ore;
					higher_priority = vec![];
				}

				if try_create.could_buy(&mut inventory, blooprint) {
					//See if this purchase will delay higher-priority purchases
					let mut delays = false;
					let mut theroetical_inventory = inventory.clone();
					if try_create.buy(&mut theroetical_inventory, blooprint) {
						let mut theroetical_robots = robots.clone();
						theroetical_robots.push(try_create.clone());
						//Advance one day ahead
						for robot in &theroetical_robots { robot.do_robot(&mut theroetical_inventory); }

						let mut projected_inventory = inventory.clone();
						//Advance one day ahead
						for robot in &robots { robot.do_robot(&mut projected_inventory); }

						for other in higher_priority {
							let wait_time = other.can_produce_in(&projected_inventory, blooprint, &robots);
							let theroetical_wait_time = other.can_produce_in(&theroetical_inventory, blooprint, &theroetical_robots);
							if theroetical_wait_time > wait_time {
								delays = true;
								break;
							}
						}
					}

					if *try_create == Robot::Ore { delays = false; }

					if !delays && try_create.buy(&mut inventory, blooprint) {
						purchase = Some(try_create.clone());
						break 'purchase;
					}
				}
			}

			if let Some(purchase) = &purchase {
				println!(" - Starting to craft {:?} robot", purchase);
				println!(" - {:?}", inventory);
			} else {
				println!(" - Did not start crafting");
			}

			//Advance time
			for robot in &robots { robot.do_robot(&mut inventory); }
			println!(" - Robots produced");
			println!(" - {:?}", inventory);

			//Complete purchase
			if let Some(new_robot) = purchase {
				println!(" - {:?} robot completed", new_robot);
				robots.push(new_robot);
			}
		}

		let score = inventory.geodes * *id;
		(*id, (brute_force_score, score))
	}).to_map();

	println!("Score sum from brute_force: {}", scores.iter().map(|(_, (score, _))| *score).sum::<usize>());
	println!("Score sum from algo: {}", scores.iter().map(|(_, (_, score))| *score).sum::<usize>());


	println!("\n\n\n\n\n\n\n\n\n\n\n\nBEGIN PART 2");


	let blooprints = vec![(1, blooprints[&1].clone()), (2, blooprints[&2].clone()), (3, blooprints[&3].clone())].into_iter().to_map();
	let scores: BTreeMap<usize, usize> = blooprints.iter().map(|(id, blooprint)| {
		let mut nodes: BTreeMap<Node, usize> = BTreeMap::new();
		let initial_robots = RobotArray { ore: 1, clay: 0, obsidian: 0, geode: 0 };
		nodes.insert(Node { robots: initial_robots.clone() }, 0);
		let mut states = vec![State {
			robots: initial_robots,
			inventory: Inventory::default(),
			purchased_on_minute: usize::MAX,
		}];
		for minute in 0..32 {
			let tasks = replace(&mut states, Vec::new());
			let new_states: BTreeSet<_> = (
				tasks.par_iter().map(|state| {
					let purchases = [Robot::Geode, Robot::Obsidian, Robot::Clay, Robot::Ore];
					let mut branches = purchases.iter().filter_map(|purchase| {
						let mut inventory = state.inventory.clone();
						if purchase.buy(&mut inventory, blooprint) {
							let robots = state.robots.clone();
							Some((State { inventory, robots, purchased_on_minute: minute }, Some(purchase)))
						}
						else { Some((state.clone(), None)) }
					}).vec();
					//Inaction branch:
					if branches.is_empty() { branches.push((state.clone(), None)); }

					//Move forward in time
					let new_states = branches.into_iter().map(|(mut branch, purchase)| {
						branch.robots.do_robots(&mut branch.inventory);
						if let Some(robot) = purchase { branch.robots.add(robot.clone()); }
						branch
					}).vec();

					new_states
				})
				.collect::<Vec<_>>().into_iter()
				.map(|v| v.into_iter())
				.flatten()
				.filter(|new_state| {
					if new_state.purchased_on_minute == minute {
						let node = Node { robots: new_state.robots.clone() };
						let previous_time = nodes.get(&node).cloned().unwrap_or(usize::MAX);
						if minute < previous_time { nodes.insert(node, minute); }
						minute <= previous_time
					}
					else { true }
				})
			).collect();
			states.extend(new_states.into_iter());
			
			let limit = 5000000;
			if states.len() > limit {
				states.sort_by_key(|state| state.inventory.clone());
				states.reverse();
				states.drain(limit..);
			}
			println!("End of minute {}; {} states", minute + 1, states.len());
		}
		let best = states.iter().max_by_key(|state| state.inventory.geodes).unwrap();
		println!("Best state for blooprint {}: {:?}", id, best);
		(*id, best.inventory.geodes)
	}).to_map();

	let values = scores.values().cloned().vec();
	assert!(values.len() == 3);
	println!("{:?}", scores.iter().vec());
	println!("{}", values.iter().cloned().product::<usize>());
}

#[derive(Debug, Clone, Copy)]
struct Blooprint {
	ore_robot_ore_cost: usize,
	clay_robot_ore_cost: usize,
	obsidian_robot_ore_cost: usize,
	obsidian_robot_clay_cost: usize,
	geode_robot_ore_cost: usize,
	geode_robot_obsidian_cost: usize,
}
impl Blooprint {
	fn max_robots_needed(&self) -> RobotArray {
		RobotArray {
			ore: [self.ore_robot_ore_cost, self.clay_robot_ore_cost, self.obsidian_robot_ore_cost, self.geode_robot_ore_cost].into_iter().max().unwrap(),
			clay: self.obsidian_robot_ore_cost,
			obsidian: self.geode_robot_ore_cost,
			geode: usize::MAX,
		}
	}
}

#[derive(Debug, Default, Clone, PartialEq, Eq)]
struct Inventory {
	ore: usize,
	clay: usize,
	obsidian: usize,
	geodes: usize,
}
impl PartialOrd<Inventory> for Inventory {
	fn partial_cmp(&self, other: &Inventory) -> Option<Ordering> { Some(self.cmp(other)) }
}
impl Ord for Inventory {
	fn cmp(&self, other: &Self) -> Ordering {
		if self.geodes > 0 || other.geodes > 0 { self.geodes.cmp(&other.geodes) }
		else if self.obsidian > 0 || other.obsidian > 0 { self.obsidian.cmp(&other.obsidian) }
		else if self.clay > 0 || other.clay > 0 { self.clay.cmp(&other.clay) }
		else { self.ore.cmp(&other.ore) }
	}
}

#[derive(Debug, Clone, Eq, PartialEq, Ord, PartialOrd)]
enum Robot {
	Ore,
	Clay,
	Obsidian,
	Geode
}
impl Robot {
	fn do_robot(&self, inventory: &mut Inventory) {
		match self {
			Robot::Ore => { inventory.ore += 1; },
			Robot::Clay => { inventory.clay += 1; },
			Robot::Obsidian => { inventory.obsidian += 1; },
			Robot::Geode => { inventory.geodes += 1; },
		}
	}
	fn buy(&self, inventory: &mut Inventory, blooprint: &Blooprint) -> bool {
		match self {
			Robot::Geode => if inventory.obsidian >= blooprint.geode_robot_obsidian_cost && inventory.ore >= blooprint.geode_robot_ore_cost {
				inventory.obsidian -= blooprint.geode_robot_obsidian_cost;
				inventory.ore -= blooprint.geode_robot_ore_cost;
				true
			} else { false },
			Robot::Obsidian => if inventory.clay >= blooprint.obsidian_robot_clay_cost && inventory.ore >= blooprint.obsidian_robot_ore_cost {
				inventory.clay -= blooprint.obsidian_robot_clay_cost;
				inventory.ore -= blooprint.obsidian_robot_ore_cost;
				true
			} else { false },
			Robot::Clay => if inventory.ore >= blooprint.clay_robot_ore_cost {
				inventory.ore -= blooprint.clay_robot_ore_cost;
				true
			} else { false },
			Robot::Ore => if inventory.ore >= blooprint.ore_robot_ore_cost {
				inventory.ore -= blooprint.ore_robot_ore_cost;
				true
			} else { false },
		}
	}
	fn can_produce_in(&self, inventory: &Inventory, blooprint: &Blooprint, robots: &Vec<Robot>) -> f64 {
		let array = RobotArray::calc(robots);
		match self {
			Robot::Ore => { blooprint.ore_robot_ore_cost.checked_sub(inventory.ore).unwrap_or(0) as f64 / array.ore as f64 },
			Robot::Clay => { blooprint.clay_robot_ore_cost.checked_sub(inventory.ore).unwrap_or(0) as f64 / array.ore as f64 },
			Robot::Obsidian => {
				( blooprint.obsidian_robot_ore_cost.checked_sub(inventory.ore).unwrap_or(0) as f64 / array.ore as f64 )
				.max( blooprint.obsidian_robot_clay_cost.checked_sub(inventory.clay).unwrap_or(0) as f64 / array.clay as f64 )
			},
			Robot::Geode => {
				( blooprint.geode_robot_ore_cost.checked_sub(inventory.ore).unwrap_or(0) as f64 / array.ore as f64 )
				.max( blooprint.geode_robot_obsidian_cost.checked_sub(inventory.obsidian).unwrap_or(0) as f64 / array.obsidian as f64 )
			}
		}
	}
	fn could_buy(&self, inventory: &Inventory, blooprint: &Blooprint) -> bool { self.buy(&mut inventory.clone(), blooprint) }

	fn ore_cost(&self, blooprint: &Blooprint) -> usize {
		match self {
			Self::Ore => blooprint.ore_robot_ore_cost,
			Self::Clay => blooprint.clay_robot_ore_cost,
			Self::Obsidian => blooprint.obsidian_robot_ore_cost,
			Self::Geode => blooprint.geode_robot_ore_cost,
		}
	}
}

#[derive(Debug, Clone, Default, Ord, PartialOrd, Eq, PartialEq)]
struct RobotArray {
	ore: usize,
	clay: usize,
	obsidian: usize,
	geode: usize,
}
impl RobotArray {
	fn calc(robots: &Vec<Robot>) -> RobotArray {
		let mut array = RobotArray::default();
		for robot in robots {
			match robot {
				Robot::Ore => { array.ore += 1; },
				Robot::Clay => { array.clay += 1; },
				Robot::Obsidian => { array.obsidian += 1; },
				Robot::Geode => { array.geode += 1; },
			}
		}
		array
	}
	fn do_robots(&self, inventory: &mut Inventory) {
		inventory.ore += self.ore;
		inventory.clay += self.clay;
		inventory.obsidian += self.obsidian;
		inventory.geodes += self.geode;
	}
	fn add(&mut self, robot: Robot) {
		match robot {
			Robot::Ore => { self.ore += 1; },
			Robot::Clay => { self.clay += 1; },
			Robot::Obsidian => { self.obsidian += 1; },
			Robot::Geode => { self.geode += 1; },
		}
	}
}

#[derive(Debug, Clone, Copy, PartialOrd, Ord, Eq, PartialEq)]
struct ResourceIntegration {
	ore: usize,
	clay: usize,
	obsidian: usize,
	geode: usize
}

#[derive(Debug, Clone, PartialOrd, Ord, Eq, PartialEq)]
struct State {
	robots: RobotArray,
	inventory: Inventory,
	purchased_on_minute: usize,
}

#[derive(Debug, Clone, PartialOrd, Eq, PartialEq, Ord)]
struct Node {
	robots: RobotArray,
}
