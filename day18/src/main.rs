use std::path::PathBuf;
use std::mem::replace;
use std::io::{Read, Write};
use std::fs::File;
use std::collections::{BTreeMap, VecDeque, HashSet, HashMap};
use regex::{Regex, Match};
use anyhow::{Result, Context, anyhow};
use std::str::Chars;
use std::iter::Peekable;
use std::cmp::Ordering;
use rayon::prelude::*;
use std::collections::BTreeSet;

mod utils;
use utils::*;

const ARENA_WIDTH: usize = 7;

fn main() {
	let input: String = {
		let input_path = std::env::args().nth(1).unwrap_or("input.txt".to_string());
		let mut input_file = File::open(input_path).expect("Failed to open the input file");
		let mut input = String::new();
		input_file.read_to_string(&mut input).expect("Failed to read the input");
		input = input.trim().to_string();
		input
	};

	let lines = input.lines().collect::<Vec<_>>();

	let cubes = lines.iter().map(|l| Pos::from_line(l)).vec();
	let mut faces = BTreeSet::new();
	for cube in &cubes {
		for face in FACES {
			let facepos = FacePos { pos: *cube, face };
			let oposite = facepos.oposite();
			if faces.contains(&oposite) { faces.remove(&oposite); }
			else { faces.insert(facepos); }
		}
	}
	println!("Faecs: {:?}", faces);
	println!("Surface area: {}", faces.len());

	let cubes: BTreeSet<_> = cubes.into_iter().collect();
	let min_extent = cubes.iter().cloned().reduce(|a, b| Pos(a.0.min(b.0), a.1.min(b.1), a.2.min(b.2))).unwrap();
	let max_extent = cubes.iter().cloned().reduce(|a, b| Pos(a.0.max(b.0), a.1.max(b.1), a.2.max(b.2))).unwrap();
	let guarenteed_outside: BTreeSet<_> = faces.iter().filter(|facepos| {
		let mut pos = facepos.pos + facepos.face.vector();
		let mut is_outside = true;
		while pos.0 >= min_extent.0 && pos.1 >= min_extent.1 && pos.2 >= min_extent.2
			&& pos.0 <= max_extent.0 && pos.1 <= max_extent.1 && pos.2 <= max_extent.2
		{
			if cubes.contains(&pos) {
				is_outside = false;
				break;
			}
			pos = pos + facepos.face.vector();
		}
		is_outside
	}).cloned().collect();
	println!("guarenteed surfae faces: {}", guarenteed_outside.len());

	let mut surface_walkers = guarenteed_outside.iter().cloned().vec();
	let mut surface_faces = guarenteed_outside;
	while !surface_walkers.is_empty() {
		/*println!("IteratioN");
		println!("{:?}", surface_walkers);
		println!("Surface area: {}", surface_faces.len());*/
		for facepos in replace(&mut surface_walkers, Vec::new()) {
			let above_pos = facepos.pos + facepos.face.vector();
			if cubes.contains(&above_pos) { panic!("Why;"); }
			for face in FACES {
				let test_oposite = FacePos { pos: above_pos, face };
				let test_facepos = test_oposite.oposite();
				if !surface_faces.contains(&test_facepos) {
					if faces.contains(&test_facepos) {
						if !cubes.contains(&test_facepos.pos) { panic!("Why 2;"); }
						surface_walkers.push(test_facepos);
						surface_faces.insert(test_facepos);
					} else {
						let adjacent_above = test_facepos.pos;
						if !cubes.contains(&adjacent_above) {
							//let adjacent_test_oposite = FacePos { pos: adjacent_above, face };
							//let adjacent_test_facepos = adjacent_test_oposite.oposite();
							let adjacent_test_facepos = FacePos { pos: adjacent_above - facepos.face.vector(), face: facepos.face };
							if !surface_faces.contains(&adjacent_test_facepos) {
								if faces.contains(&adjacent_test_facepos) {
									if !cubes.contains(&adjacent_test_facepos.pos) { panic!("Why 3;"); }
									surface_walkers.push(adjacent_test_facepos);
									surface_faces.insert(adjacent_test_facepos);
								} else {
									let around_the_side_facepos = FacePos { pos: facepos.pos, face };
									if !surface_faces.contains(&around_the_side_facepos) && faces.contains(&around_the_side_facepos) {
										if cubes.contains(&around_the_side_facepos.oposite().pos) { panic!("Why 3;"); }
										surface_walkers.push(around_the_side_facepos);
										surface_faces.insert(around_the_side_facepos);
									}
								}
							}
						} else {
							panic!("Why 4 {:?} => {:?} to {:?} => {:?}", facepos, above_pos, face, test_facepos);
						}
					}
				}
			}
		}
	}
	println!("Surface area: {}", surface_faces.len());
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
enum Face {
	Top,
	Bottom,
	North,
	South,
	East,
	West
}
impl Face {
	fn oposite(&self) -> Face {
		match self {
			Top => Bottom,
			Bottom => Top,
			North => South,
			South => North,
			East => West,
			West => East,

		}
	}
	fn vector(&self) -> Pos {
		match self {
			Top => Pos(0, 1, 0),
			Bottom => Pos(0, -1, 0),
			North => Pos(0, 0, -1),
			South => Pos(0, 0, 1),
			East => Pos(1, 0, 0),
			West => Pos(-1, 0, 0),
		}
	}
}
use Face::*;

const FACES: [Face; 6] = [Top, Bottom, North, South, East, West];

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
struct Pos(i64, i64, i64);
impl Pos {
	fn from_line(line: impl AsRef<str>) -> Self {
		let bits = line.as_ref().split(",").map(|n| n.parse::<i64>().unwrap()).vec();
		assert!(bits.len() == 3);
		Pos(bits[0], bits[1], bits[2])
	}
}
impl std::ops::Add<Pos> for Pos {
	type Output = Pos;
	fn add(self, o: Pos) -> Self::Output { Pos(self.0 + o.0, self.1 + o.1, self.2 + o.2) }
}
impl std::ops::Sub<Pos> for Pos {
	type Output = Pos;
	fn sub(self, o: Pos) -> Self::Output { Pos(self.0 - o.0, self.1 - o.1, self.2 - o.2) }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
struct FacePos {
	pos: Pos,
	face: Face,
}
impl FacePos {
	fn oposite(&self) -> FacePos {
		FacePos {
			pos: self.pos + self.face.vector(),
			face: self.face.oposite(),
		}
	}
}
